<?php
namespace frontend\controllers;

use Yii;
use yii\rest\ActiveController;

/**
 * Site controller
 */
class GoodsComplimentsController extends ApiController
{
	public $modelClass = 'common\models\GoodsCompliments';
}
