<?php
namespace frontend\controllers;

use League\Glide\Api\Api;
use Yii;
use yii\rest\ActiveController;

/**
 * Site controller
 */
class VacansiesController extends ApiController
{
	public $modelClass = 'common\models\Vacancies';

}
