<?php
namespace frontend\controllers;

use Yii;
use yii\rest\ActiveController;

/**
 * Site controller
 */
class CallbackController extends ApiController
{
	public $modelClass = 'common\models\Callback';

}
