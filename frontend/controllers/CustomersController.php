<?php
namespace frontend\controllers;

use Yii;
use yii\rest\ActiveController;

/**
 * Site controller
 */
class CustomersController extends ApiController
{
	public $modelClass = 'common\models\Customers';

}
