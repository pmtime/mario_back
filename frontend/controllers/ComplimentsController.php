<?php
namespace frontend\controllers;

use Yii;
use yii\rest\ActiveController;

/**
 * Site controller
 */
class ComplimentsController extends ApiController
{
	public $modelClass = 'common\models\Compliments';
}
