<?php
namespace frontend\controllers;

use Yii;
use yii\rest\ActiveController;

/**
 * Site controller
 */
class CategoriesController extends ApiController
{
	public $modelClass = 'common\models\Categories';

}
