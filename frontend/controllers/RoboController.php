<?php
namespace frontend\controllers;

use Yii;
use yii\rest\Controller;

/**
 * Site controller
 */
class RoboController extends Controller
{
	
	public function behaviors()
	{
		$behaviors = parent::behaviors();

		// remove authentication filter
		$auth = $behaviors['authenticator'];
		unset($behaviors['authenticator']);

		// add CORS filter
		$behaviors['corsFilter'] = [
			'class' => \yii\filters\Cors::className(),
		];

		// re-add authentication filter
		$behaviors['authenticator'] = $auth;
		// avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
		$behaviors['authenticator']['except'] = ['options'];

		return $behaviors;
	}

	public function actionIndex($order_id,$summ){
		$mrh_login = "mariodostavka";
		$mrh_pass1 = "OeL1119xLvWVbjm0rSuu";
		// номер заказа
		// number of order
		$inv_id = $order_id;
		// описание заказа
		// order description
		$inv_desc = "ROBOKASSA Advanced User Guide";
		// сумма заказа
		// sum of order
		$out_summ = $summ;
		// тип товара
		// code of goods
		$shp_item = "2";
		// предлагаемая валюта платежа
		// default payment e-currency
		$in_curr = "RUB";
		// язык
		// language
		$culture = "ru";
		// формирование подписи
		// generate signature
		$crc  = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item");

		return compact('mrh_login','mrh_pass1','inv_id','inv_desc','out_summ','shp_item','in_curr','culture','crc');
	}
}
