<?php
return [
    'class'=>'yii\web\UrlManager',
    'enablePrettyUrl'=>true,
    'showScriptName'=>false,
    'rules'=> [
        // Pages
        ['pattern'=>'page/<slug>', 'route'=>'page/view'],

        // Articles
        ['pattern'=>'article/index', 'route'=>'article/index'],
        ['pattern'=>'article/attachment-download', 'route'=>'article/attachment-download'],
        ['pattern'=>'article/<slug>', 'route'=>'article/view'],

        // Api
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/article', 'only' => ['index', 'view', 'options']],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/user', 'only' => ['index', 'view', 'options']],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'goods'],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'promocodes'],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'vacansies'],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'vacansies-answers'],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'config'],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'goods-compliments'],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'robo'],
        ['class' => 'yii\rest\UrlRule', 'controller' => ['customers','callback']],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'compliments','only' => ['index', 'view', 'options','create']],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'orders'],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'ordered-products'],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'history', 'pluralize'=>false],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'payment-history', 'pluralize'=>false]

    ]
];
