<?php

	use common\models\GoodsPriceType;
	use yii\db\Migration;

class m160712_082310_add_fields extends Migration
{
    public function up()
    {
		$this->addColumn(GoodsPriceType::tableName(),'param','VARCHAR(255)');
		$this->addColumn(GoodsPriceType::tableName(),'value','VARCHAR(255)');
    }

    public function down()
    {
	    $this->dropColumn(GoodsPriceType::tableName(),'param');
	    $this->dropColumn(GoodsPriceType::tableName(),'value');
    }
}
