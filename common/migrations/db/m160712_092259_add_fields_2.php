<?php

	use common\models\GoodsComplimentsPromocode;
	use yii\db\Migration;

class m160712_092259_add_fields_2 extends Migration
{
    public function up()
    {
		$this->addColumn(GoodsComplimentsPromocode::tableName(),'count','VARCHAR(255)');
    }

    public function down()
    {
       $this->dropColumn(GoodsComplimentsPromocode::tableName(),'count');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
