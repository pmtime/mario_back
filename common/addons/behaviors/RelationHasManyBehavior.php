<?php

	namespace common\addons\behaviors;

	use Yii;
	use yii\base\Behavior;
	use yii\db\ActiveRecord;
	use yii\base\ErrorException;
	use yii\db\Exception;
	/**
	 * Class RelationHasManyBehaviour
	 */
	class RelationHasManyBehavior extends Behavior
	{
		/**
		 * Stores a list of relations, affected by the behavior. Configurable property.
		 * @var array
		 */
		public $relations;

		/**
		 * Stores values of relation attributes. All entries in this array are considered
		 * dirty (changed) attributes and will be saved in saveRelations().
		 * @var array
		 */
		private $_values = [];

		/**
		 * Used to store fields that this behavior creates. Each field refers to a relation
		 * and has optional getters and setters.
		 * @var array
		 */
		private $_fields = [];

		/**
		 * Events list
		 * @return array
		 */
		public function events()
		{
			return [
				ActiveRecord::EVENT_AFTER_INSERT => 'saveRelations',
				ActiveRecord::EVENT_AFTER_UPDATE => 'saveRelations',
			];
		}
		/**
		 * Create and save all related Models
		 * @param $event
		 * @throws ErrorException
		 * @throws Exception
		 */
		public function saveRelations($event)
		{
			/**
			 * @var $primaryModel \yii\db\ActiveRecord
			 */
			$primaryModel = $this->owner;

			if (is_array($primaryModelPk = $primaryModel->getPrimaryKey())) {
				throw new ErrorException("This behavior does not support composite primary keys");
			}

			// Save relations data
			foreach ($this->relations as $attributeName => $params) {

				$relationName = $this->getRelationName($attributeName);
				$relation = $primaryModel->getRelation($relationName);
				$an = $this->owner->$attributeName;
				// has_many
				if (!empty($relation->link) && $relation->multiple) {
					list($manyTableFkColumn) = array_keys($relation->link);
					//HasMany, primary model HAS MANY foreign models, must update foreign model table
					$foreignModel = new $relation->modelClass();
					$manyTable = $foreignModel->tableName();

					$connection = $foreignModel::getDb();
					$transaction = $connection->beginTransaction();

					$defaultValue = $this->owner->id;
					$relatedColumns = [];
					foreach($foreignModel->getTableSchema()->columns as $coll){
						if($coll->name != 'id'){
							$relatedColumns[] = $coll->name;
						}
					}
					
					$newAttr = [];
					$appendFK = [$manyTableFkColumn=>$defaultValue];
					foreach ($this->owner->$attributeName as $i=>$atr){
							if(is_int($i)){
								$newAttr += array_fill_keys([$i],array_merge(array_flip($relatedColumns),array_merge($atr,$appendFK)));
							}else{
								foreach ($atr as $a){
									$as = [$i => $a];
									$newAttr[] = array_merge(array_flip($relatedColumns),array_merge($as,$appendFK));
								}
							}
						}
					try {
						//Delete all owner ralations

						$connection
							->createCommand()
							->delete($manyTable, "{$manyTableFkColumn} = {$defaultValue}")
							->execute();
						//Add all owner relations

						$connection
							->createCommand()
							->batchInsert(
								$manyTable,
								$relatedColumns,
								$newAttr
							)
							->execute();

						$transaction->commit();
					} catch (Exception $ex) {
						$transaction->rollback();
						throw $ex;
					}

				} else {
					throw new ErrorException('Relationship type not supported.');
				}
			}
		}
		/**
		 * Get name of a relation
		 * @param string $attributeName
		 * @return null
		 */
		private function getRelationName($attributeName)
		{
			$params = $this->getRelationParams($attributeName);

			if (is_string($params)) {
				return $params;
			} elseif (is_array($params) && !empty($params[0])) {
				return $params[0];
			}

			return null;
		}

		/**
		 * Get parameters of a relation
		 * @param string $attributeName
		 * @return mixed
		 * @throws ErrorException
		 */
		private function getRelationParams($attributeName)
		{
			if (empty($this->relations[$attributeName])) {
				throw new ErrorException("Parameter \"{$attributeName}\" does not exist");
			}

			return $this->relations[$attributeName];
		}
	}
