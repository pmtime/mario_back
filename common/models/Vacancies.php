<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%vacancies}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $functions
 * @property string $offer
 * @property integer $compensation_from
 * @property integer $compensation_to
 * @property integer $author_id
 * @property integer $updater_id
 * @property string $create_time
 * @property string $update_time
 * @property integer $status_id
 *
 * @property Statuses $status
 * @property VacanciesAnswers[] $vacanciesAnswers
 */
class Vacancies extends \yii\db\ActiveRecord
{
                                                    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vacancies}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                                                                                                                                                                
            [
             'class' => BlameableBehavior::className(),
             'createdByAttribute' => 'author_id',
             'updatedByAttribute' => 'updater_id',
            ],
                                                                                                            
            [
             'class' => TimestampBehavior::className(),
             'createdAtAttribute' => 'create_time',
             'updatedAtAttribute' => 'update_time',
             'value' => new Expression('NOW()'),
            ],
                                                                                                                        
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['functions', 'offer'], 'string'],
            [['compensation_from', 'compensation_to', 'status_id'], 'integer'],
            [['status_id'], 'required'],
            [['label'], 'string', 'max' => 255],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Statuses::className(), 'targetAttribute' => ['status_id' => 'id']],
                                                                                                                                                                                                                                                    ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Вакансия',
            'functions' => 'Обязанности',
            'offer' => 'Предлагаем',
            'compensation_from' => 'З/П от',
            'compensation_to' => 'З/П до',
            'author_id' => 'Author ID',
            'updater_id' => 'Updater ID',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
            'status_id' => 'Статус',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\VacanciesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\VacanciesQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){ 
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getLabel()
    {
        if(!empty($this->label)){ 
            return $this->label;
        }else{
            throw new UnknownPropertyException($this->label.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getFunctions()
    {
        if(!empty($this->functions)){ 
            return $this->functions;
        }else{
            throw new UnknownPropertyException($this->functions.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getOffer()
    {
        if(!empty($this->offer)){ 
            return $this->offer;
        }else{
            throw new UnknownPropertyException($this->offer.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getCompensation_from()
    {
        if(!empty($this->compensation_from)){ 
            return $this->compensation_from;
        }else{
            throw new UnknownPropertyException($this->compensation_from.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getCompensation_to()
    {
        if(!empty($this->compensation_to)){ 
            return $this->compensation_to;
        }else{
            throw new UnknownPropertyException($this->compensation_to.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getAuthor_id()
    {
        if(!empty($this->author_id)){ 
            return $this->author_id;
        }else{
            throw new UnknownPropertyException($this->author_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getUpdater_id()
    {
        if(!empty($this->updater_id)){ 
            return $this->updater_id;
        }else{
            throw new UnknownPropertyException($this->updater_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getCreate_time()
    {
        if(!empty($this->create_time)){ 
            return $this->create_time;
        }else{
            throw new UnknownPropertyException($this->create_time.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getUpdate_time()
    {
        if(!empty($this->update_time)){ 
            return $this->update_time;
        }else{
            throw new UnknownPropertyException($this->update_time.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getStatus_id()
    {
        if(!empty($this->status_id)){ 
            return $this->status_id;
        }else{
            throw new UnknownPropertyException($this->status_id.', пуст в записи'. $this->id);
        }
    }
        
    /**
    * Relations
    **/

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedStatus()
    {
    return $this->hasOne(Statuses::className(), ['id' => 'status_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedVacanciesAnswers()
    {
    return $this->hasMany(VacanciesAnswers::className(), ['vacancy_id' => 'id']);
    }

    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'label' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],
    'functions' => [
        'type' => ActiveFormBuilder::INPUT_WIDGET,
        'widgetClass' => \yii\imperavi\Widget::className(),
        'options'=>[
            'plugins' => ['fullscreen', 'fontcolor', 'video'],
            'options'=>[
                'minHeight' => 150,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => false,
                'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
            ]
        ]
    ],
    'offer' => [
        'type' => ActiveFormBuilder::INPUT_WIDGET,
        'widgetClass' => \yii\imperavi\Widget::className(),
        'options'=>[
            'plugins' => ['fullscreen', 'fontcolor', 'video'],
            'options'=>[
                'minHeight' => 150,
                'maxHeight' => 400,
                'buttonSource' => true,
                'convertDivs' => false,
                'removeEmptyTags' => false,
                'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
            ]
        ]
    ],
    'compensation_from' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],
    'compensation_to' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],

    'status_id' => [
	    'type'    => ActiveFormBuilder::INPUT_DROPDOWN_LIST ,
	    'items'   => ArrayHelper::map ( Statuses::find ()->andFilterWhere(['class'=>self::className()])->all () , 'id' , 'label' ) ,
	    'defaultValue'=>3,
    ] ,
    
];}}
