<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%deliveries}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $author_id
 * @property integer $updater_id
 * @property integer $create_time
 * @property integer $update_time
 *
 * @property Orders[] $orders
 */
class Deliveries extends \yii\db\ActiveRecord
{
                            
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%deliveries}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                
            [
             'class' => BlameableBehavior::className(),
             'createdByAttribute' => 'author_id',
             'updatedByAttribute' => 'updater_id',
            ],
                                                                                                            
            [
             'class' => TimestampBehavior::className(),
             'createdAtAttribute' => 'create_time',
             'updatedAtAttribute' => 'update_time',
             'value' => new Expression('NOW()'),
            ],
                                                                        
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['label'], 'string', 'max' => 255],
                                                                                                                                                ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Название',
            'author_id' => 'Author ID',
            'updater_id' => 'Updater ID',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\DeliveriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\DeliveriesQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){ 
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getLabel()
    {
        if(!empty($this->label)){ 
            return $this->label;
        }else{
            throw new UnknownPropertyException($this->label.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getAuthor_id()
    {
        if(!empty($this->author_id)){ 
            return $this->author_id;
        }else{
            throw new UnknownPropertyException($this->author_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getUpdater_id()
    {
        if(!empty($this->updater_id)){ 
            return $this->updater_id;
        }else{
            throw new UnknownPropertyException($this->updater_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getCreate_time()
    {
        if(!empty($this->create_time)){ 
            return $this->create_time;
        }else{
            throw new UnknownPropertyException($this->create_time.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getUpdate_time()
    {
        if(!empty($this->update_time)){ 
            return $this->update_time;
        }else{
            throw new UnknownPropertyException($this->update_time.', пуст в записи'. $this->id);
        }
    }
        
    /**
    * Relations
    **/

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedOrders()
    {
    return $this->hasMany(Orders::className(), ['delivery_id' => 'id']);
    }

    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'label' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],    
    
    
    
    
];}}
