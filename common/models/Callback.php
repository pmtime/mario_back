<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%callback}}".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $create_time
 * @property string $update_time
 *
 * @property Customers $customer
 * @property Customers $customers
 */
class Callback extends \yii\db\ActiveRecord
{
                        
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%callback}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                                
            [
             'class' => TimestampBehavior::className(),
             'createdAtAttribute' => 'create_time',
             'updatedAtAttribute' => 'update_time',
             'value' => new Expression('NOW()'),
            ],
                                                                                    
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['customer_id'], 'required'],
            [['customer_id'], 'integer'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'id']],
                                                                                                        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Клиент',
            'create_time' => 'Время',
            'update_time' => 'Update Time',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\CallbackQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\CallbackQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){ 
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getCustomer_id()
    {
        if(!empty($this->customer_id)){ 
            return $this->customer_id;
        }else{
            throw new UnknownPropertyException($this->customer_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getCreate_time()
    {
        if(!empty($this->create_time)){ 
            return $this->create_time;
        }else{
            throw new UnknownPropertyException($this->create_time.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getUpdate_time()
    {
        if(!empty($this->update_time)){ 
            return $this->update_time;
        }else{
            throw new UnknownPropertyException($this->update_time.', пуст в записи'. $this->id);
        }
    }
        
    /**
    * Relations
    **/

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedCustomer()
    {
    return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedCustomers()
    {
    return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {

			if($this->isNewRecord){
				$order = new Orders();
				$order->createBaseOrder($this->customer_id);
				$order->save();

			}

			return true;
		}
		return false;
	}
    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'customer_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Customers::find()->andFilterWhere(['id'=>$this->customer_id])->all(),'id','fullName'),
    ],
    
    
    
];}}
