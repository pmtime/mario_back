<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%sms_solo}}".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $text
 */
class SmsSolo extends \yii\db\ActiveRecord
{
            
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sms_solo}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [
	    [
		    'class' => TimestampBehavior::className(),
		    'createdAtAttribute' => 'create_time',
		    'updatedAtAttribute' => 'update_time',
		    'value' => new Expression('NOW()'),
	    ],
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['customer_id'], 'integer'],
            [['text','create_time','update_time','status'], 'safe'],
                                                                                    ];
    }

	public function getStatus (  )
	{
		switch ($this->status){
			case 0:
				return 'Отправка';
				break;
			case 1:
				return 'Доставлено';
				break;
		}

	}
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'text' => 'Сообщение',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\SmsSoloQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\SmsSoloQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){ 
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getCustomer_id()
    {
        if(!empty($this->customer_id)){ 
            return $this->customer_id;
        }else{
            throw new UnknownPropertyException($this->customer_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getText()
    {
        if(!empty($this->text)){ 
            return $this->text;
        }else{
            throw new UnknownPropertyException($this->text.', пуст в записи'. $this->id);
        }
    }
	public function getCustomer(){
		return $this->hasOne(Customers::className(),['id'=>'customer_id']);
	}
    /**
    * Relations
    **/

    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'customer_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'text' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
];}}
