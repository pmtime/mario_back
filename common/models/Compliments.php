<?php

	namespace common\models;

	use Yii;
	use yii\behaviors\BlameableBehavior;
	use yii\behaviors\SluggableBehavior;
	use yii\behaviors\TimestampBehavior;
	use trntv\filekit\behaviors\UploadBehavior;
	use \yii\helpers\Html;
	use \yii\base\UnknownPropertyException;
	use yii\db\Expression;
	use trntv\filekit\widget\Upload;
	use metalguardian\formBuilder\ActiveFormBuilder;
	use yii\helpers\ArrayHelper;

	/**
	 * This is the model class for table "{{%compliments}}".
	 *
	 * @property integer    $id
	 * @property string     $label
	 * @property string     $price
	 * @property integer    $category_id
	 * @property integer    $author_id
	 * @property integer    $updater_id
	 * @property string     $create_time
	 * @property string     $update_time
	 * @property integer    $status_id
	 * @property string     $thumbnail_base_url
	 * @property string     $thumbnail_path
	 *
	 * @property Categories $categories
	 * @property Goods[]    $ids
	 * @property Statuses   $status
	 */
	class Compliments extends \yii\db\ActiveRecord
	{

		/**
		 * @var array
		 */
		public $thumbnail;

		/**
		 * @inheritdoc
		 */
		public static function tableName ()
		{

			return '{{%compliments}}';
		}

		/**
		 * @inheritdoc
		 */
		public function behaviors ()
		{

			return [
				[
					'class'              => BlameableBehavior::className () ,
					'createdByAttribute' => 'author_id' ,
					'updatedByAttribute' => 'updater_id' ,
				] ,

				[
					'class'              => TimestampBehavior::className () ,
					'createdAtAttribute' => 'create_time' ,
					'updatedAtAttribute' => 'update_time' ,
					'value'              => new Expression( 'NOW()' ) ,
				] ,

				[
					'class'            => UploadBehavior::className () ,
					'attribute'        => 'thumbnail' ,
					'pathAttribute'    => 'thumbnail_path' ,
					'baseUrlAttribute' => 'thumbnail_base_url' ,
				] ,

			];

		}

		/**
		 * @inheritdoc
		 */
		public function rules ()
		{

			return [

				[
					[
						'category_id' ,
						'status_id' ,
					] ,
					'required' ,
				] ,
				[
					[
						'category_id' ,
						'status_id' ,
					] ,
					'integer' ,
				] ,
				[
					[
						'label' ,
						'price' ,
					] ,
					'string' ,
					'max' => 255 ,
				] ,
				[
					[ 'status_id' ] ,
					'exist' ,
					'skipOnError'     => TRUE ,
					'targetClass'     => Statuses::className () ,
					'targetAttribute' => [ 'status_id' => 'id' ] ,
				] ,
				[
					[ 'thumbnail' ] ,
					'safe' ,
				] ,
			];
		}

		/**
		 * @inheritdoc
		 */
		public function attributeLabels ()
		{

			return [
				'id'                 => 'ID' ,
				'label'              => 'Название' ,
				'price'              => 'Стоимость' ,
				'category_id'        => 'Категория' ,
				'author_id'          => 'Author ID' ,
				'updater_id'         => 'Updater ID' ,
				'create_time'        => 'Create Time' ,
				'update_time'        => 'Update Time' ,
				'status_id'          => 'Статус' ,
				'thumbnail_base_url' => 'Thumbnail Base Url' ,
				'thumbnail_path'     => 'Thumbnail Path' ,
				'thumbnail'          => 'Изображение' ,
			];
		}

		/**
		 * @inheritdoc
		 * @return \common\models\query\ComplimentsQuery the active query used by this AR class.
		 */
		public static function find ()
		{

			return new \common\models\query\ComplimentsQuery( get_called_class () );
		}

		/**
		 * Access methods for the table attributes
		 **/

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getId ()
		{

			if ( !empty( $this->id ) ) {
				return $this->id;
			}
			else {
				throw new UnknownPropertyException( $this->id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getLabel ()
		{

			if ( !empty( $this->label ) ) {
				return $this->label;
			}
			else {
				throw new UnknownPropertyException( $this->label . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getPrice ()
		{

			if ( !empty( $this->price ) ) {
				return $this->price;
			}
			else {
				throw new UnknownPropertyException( $this->price . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getCategory_id ()
		{

			if ( !empty( $this->category_id ) ) {
				return $this->category_id;
			}
			else {
				throw new UnknownPropertyException( $this->category_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getAuthor_id ()
		{

			if ( !empty( $this->author_id ) ) {
				return $this->author_id;
			}
			else {
				throw new UnknownPropertyException( $this->author_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getUpdater_id ()
		{

			if ( !empty( $this->updater_id ) ) {
				return $this->updater_id;
			}
			else {
				throw new UnknownPropertyException( $this->updater_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getCreate_time ()
		{

			if ( !empty( $this->create_time ) ) {
				return $this->create_time;
			}
			else {
				throw new UnknownPropertyException( $this->create_time . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getUpdate_time ()
		{

			if ( !empty( $this->update_time ) ) {
				return $this->update_time;
			}
			else {
				throw new UnknownPropertyException( $this->update_time . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getStatus_id ()
		{

			if ( !empty( $this->status_id ) ) {
				return $this->status_id;
			}
			else {
				throw new UnknownPropertyException( $this->status_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getThumbnail_base_url ()
		{

			if ( !empty( $this->thumbnail_base_url ) ) {
				return $this->thumbnail_base_url;
			}
			else {
				throw new UnknownPropertyException( $this->thumbnail_base_url . ', пуст в записи' . $this->id );
			}
		}

		public function getThumbnailImage ()
		{

			return Html::img ( Yii::$app->glide->createSignedUrl ( [
				'glide/index' ,
				'path' => $this->thumbnail_path ,
				'w'    => 200 ,
			] , TRUE ) , [ 'class' => '' ] );
		}

		public function getMainImage ()
		{

			return Html::img ( Yii::$app->glide->createSignedUrl ( [
				'glide/index' ,
				'path' => $this->thumbnail_path ,
				'w'    => 800 ,
			] , TRUE ) , [ 'class' => '' ] );
		}

		public function getThumbnailSrc ()
		{

			return Yii::$app->glide->createSignedUrl ( [
				'glide/index' ,
				'path' => $this->thumbnail_path ,
				'w'    => 200 ,
			] , TRUE );
		}

		public function getMainSrc ()
		{

			return Yii::$app->glide->createSignedUrl ( [
				'glide/index' ,
				'path' => $this->thumbnail_path ,
				'w'    => 800 ,
			] , TRUE );
		}
		/**
		 * Relations
		 **/

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedCategories ()
		{

			return $this->hasOne ( Categories::className () , [ 'id' => 'category_id' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedIds ()
		{

			return $this->hasMany ( Goods::className () , [ 'category_id' => 'id' ] )->viaTable ( '{{%categories}}' , [ 'id' => 'category_id' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedStatus ()
		{

			return $this->hasOne ( Statuses::className () , [ 'id' => 'status_id' ] );
		}

		/**
		 * Admin form generator
		 **/
		public function getFormConfig ()
		{

			return [

				'label' => [
					'type' => ActiveFormBuilder::INPUT_TEXT ,
				] ,

				'price'       => [
					'type' => ActiveFormBuilder::INPUT_TEXT ,
				] ,
				'thumbnail'   => [
					'type'        => ActiveFormBuilder::INPUT_WIDGET ,
					'widgetClass' => Upload::className () ,
					'options'     => [
						'url'         => [ '/file-storage/upload' ] ,
						'maxFileSize' => 5000000 ,
						// 5 MiB
					] ,
				] ,
				'category_id' => [
					'type'    => ActiveFormBuilder::INPUT_DROPDOWN_LIST ,
					'items'   => ArrayHelper::map ( Categories::find ()->all () , 'id' , 'label' ) ,
					'options' => [
						'prompt' => 'Выберите категорию' ,
					] ,
				] ,

				'status_id' => [
					'type'    => ActiveFormBuilder::INPUT_DROPDOWN_LIST ,
					'items'   => ArrayHelper::map ( Statuses::find ()->andFilterWhere(['class'=>self::className()])->all () , 'id' , 'label' ) ,
					'defaultValue'=>3,
				] ,

			];
		}

		public function fields ()
		{

			return [
				'id' ,
				'label' ,
				'price',
			];
		}

		public function extraFields ()
		{

			return [
				'relatedCategories' ,
				'relatedStatus',
			];
		}

	}
