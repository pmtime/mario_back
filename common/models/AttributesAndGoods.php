<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%attributes_and_goods}}".
 *
 * @property integer $id
 * @property integer $good_id
 * @property integer $attribute_id
 */
class AttributesAndGoods extends \yii\db\ActiveRecord
{
            
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%attributes_and_goods}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                                    
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['good_id', 'attribute_id'], 'integer'],
                                                                                    ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'good_id' => 'Good ID',
            'attribute_id' => 'Attribute ID',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\AttributesAndGoodsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\AttributesAndGoodsQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){ 
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getGood_id()
    {
        if(!empty($this->good_id)){ 
            return $this->good_id;
        }else{
            throw new UnknownPropertyException($this->good_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getAttribute_id()
    {
        if(!empty($this->attribute_id)){ 
            return $this->attribute_id;
        }else{
            throw new UnknownPropertyException($this->attribute_id.', пуст в записи'. $this->id);
        }
    }
        
    /**
    * Relations
    **/

    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'good_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'attribute_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
];}}
