<?php

	namespace common\models;

	use common\addons\behaviors\RelationHasManyBehavior;
	use unclead\widgets\MultipleInput;
	use arogachev\ManyToMany\behaviors\ManyToManyBehavior;
	use Yii;
	use yii\behaviors\BlameableBehavior;
	use yii\behaviors\TimestampBehavior;
	use trntv\filekit\behaviors\UploadBehavior;
	use \yii\helpers\Html;
	use \yii\base\UnknownPropertyException;
	use yii\db\Expression;
	use trntv\filekit\widget\Upload;
	use metalguardian\formBuilder\ActiveFormBuilder;
	use yii\helpers\ArrayHelper;
	use himiklab\sortablegrid\SortableGridBehavior;

	/**
	 * This is the model class for table "{{%goods}}".
	 *
	 * @property integer        $id
	 * @property string         $label
	 * @property integer        $category_id
	 * @property string         $thumbnail_base_url
	 * @property string         $thumbnail_path
	 * @property integer        $attribute_id
	 * @property string         $description
	 * @property integer        $author_id
	 * @property integer        $updater_id
	 * @property string         $create_time
	 * @property string         $update_time
	 * @property integer        $status_id
	 *
	 * @property Categories     $categories
	 * @property Compliments[]  $ids
	 * @property Attributes     $attribute
	 * @property Categories     $category
	 * @property OrderedGoods   $id0
	 * @property Statuses       $status
	 * @property GoodsPriceType $price
	 * @property OrderedGoods[] $orderedGoods
	 */
	class Goods extends \yii\db\ActiveRecord
	{

		public $editableAttr = [ ];

		/**
		 * @var array
		 */
		public $thumbnail;

		/**
		 * @inheritdoc
		 */
		public static function tableName ()
		{

			return '{{%goods}}';
		}

		/**
		 * @inheritdoc
		 */
		public function behaviors ()
		{

			return [
				'sort' => [
					'class'             => SortableGridBehavior::className () ,
					'sortableAttribute' => 'sortOrder' ,
				] ,
				[
					'class'            => UploadBehavior::className () ,
					'attribute'        => 'thumbnail' ,
					'pathAttribute'    => 'thumbnail_path' ,
					'baseUrlAttribute' => 'thumbnail_base_url' ,
				] ,
				[
					'class'     => RelationHasManyBehavior::className () ,
					'relations' => [
						'relatedPrice' => 'relatedPrice' ,
					] ,
				] ,
				[
					'class'     => ManyToManyBehavior::className () ,
					'relations' => [
						[
							'editableAttribute' => 'editableAttr' ,
							// Editable attribute name
							'table'             => 'tbl_attributes_and_goods' ,
							// Name of the junction table
							'ownAttribute'      => 'good_id' ,
							// Name of the column in junction table that represents current model
							'relatedModel'      => Attributes::className () ,
							// Related model class
							'relatedAttribute'  => 'attribute_id' ,
							// Name of the column in junction table that represents related model
						] ,
					] ,
				] ,
				[
					'class'              => BlameableBehavior::className () ,
					'createdByAttribute' => 'author_id' ,
					'updatedByAttribute' => 'updater_id' ,
				] ,

				[
					'class'              => TimestampBehavior::className () ,
					'createdAtAttribute' => 'create_time' ,
					'updatedAtAttribute' => 'update_time' ,
					'value'              => new Expression( 'NOW()' ) ,
				] ,

			];

		}

		/**
		 * @inheritdoc
		 */
		public function rules ()
		{

			return [
				[
					[
						'relatedPrice' ,
						'editableAttr' ,
						'thumbnail' ,
						'category_id' ,
						'status_id' ,
					] ,
					'safe' ,
				] ,
				[
					[
						'category_id' ,
						'status_id' ,
						'label' ,
						'description' ,
						'editableAttr' ,
					] ,
					'required' ,
				] ,
				[
					[ 'description' ] ,
					'string' ,
				] ,
				[
					[ 'label' ] ,
					'string' ,
					'max' => 255 ,
				] ,
			];
		}

		/**
		 * @inheritdoc
		 */
		public function attributeLabels ()
		{

			return [
				'id'                 => 'ID' ,
				'label'              => 'Название' ,
				'category_id'        => 'Категория' ,
				'thumbnail_base_url' => 'Изображение' ,
				'thumbnail_path'     => 'Thumbnail Path' ,
				'attribute_id'       => 'Доп.параметр' ,
				'description'        => 'Описание' ,
				'author_id'          => 'Author ID' ,
				'updater_id'         => 'Updater ID' ,
				'create_time'        => 'Create Time' ,
				'update_time'        => 'Update Time' ,
				'status_id'          => 'Статус' ,
				'editableAttr'       => 'Допы' ,
				'thumbnail'          => 'Изображение' ,
				'relatedPrice'       => 'Ценовая политика' ,
				'priority'           => 'Приоритет' ,
			];
		}

		public static function getGoodCalculatedPrice ( $good_id , $price_id , $count , $compliments = [ ] , $promocode_id = FALSE )
		{

			$promo = ( $promocode_id ) ? Promocodes::findOne ( [ 'id' => $promocode_id ] ) : FALSE;

			$goodPrice = self::find ()->andWhere ( [ 'id' => $good_id ] )->one ()->getRelatedPrice ()->andFilterWhere ( [ 'id' => $price_id ] )->one ()->price;

			$additionalPrice = [ ];
			$good_price = $goodPrice * $count;

			if ( empty( $compliments[ 0 ][ 'good_id' ] ) ) {
				if ( $promo ) {
					$type = ( strpos ( $promo->sale , '%' ) === FALSE ) ? 'int' : '%';

					return ( $type == '%' ) ? ( $promo ) ? $good_price * ( ( 100 - $promo->sale ) / 100 ) : $good_price : $good_price - $promo->sale;
				}
				else {
					return $good_price;
				}

			}
			else {
				foreach ( $compliments as $compliment ) {
					if ( $compliment[ 'good_id' ] == $good_id ) {
						$findComplimentPrice = Compliments::find ()->andWhere ( [ 'id' => $compliment[ 'compliments_id' ] ] )->one ();

						$calculateComplimentPrice = $findComplimentPrice->price * $compliment[ 'count' ];
						$additionalPrice[] += $calculateComplimentPrice;
					}
				}
				$goodComplimentPrice = array_sum ( $additionalPrice );
				$price = $good_price + $goodComplimentPrice;
				if ( $promo ) {
					$type = ( $promo ) ? ( strpos ( $promo->sale , '%' ) === FALSE ) ? 'int' : '%' : FALSE;

					return ( $type == '%' ) ? ( $promo ) ? $price * ( ( 100 - $promo->sale ) / 100 ) : $price : $price - $promo->sale;
				}
				else {
					return $price;
				}

			}

		}

		/**
		 * @inheritdoc
		 * @return \common\models\query\GoodsQuery the active query used by this AR class.
		 */
		public static function find ()
		{

			return new \common\models\query\GoodsQuery( get_called_class () );
		}

		/**
		 * Access methods for the table attributes
		 **/

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getId ()
		{

			if ( !empty( $this->id ) ) {
				return $this->id;
			}
			else {
				throw new UnknownPropertyException( $this->id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getLabel ()
		{

			if ( !empty( $this->label ) ) {
				return $this->label;
			}
			else {
				throw new UnknownPropertyException( $this->label . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getCategory_id ()
		{

			if ( !empty( $this->category_id ) ) {
				return $this->category_id;
			}
			else {
				throw new UnknownPropertyException( $this->category_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getThumbnail_base_url ()
		{

			if ( !empty( $this->thumbnail_base_url ) ) {
				return $this->thumbnail_base_url;
			}
			else {
				throw new UnknownPropertyException( $this->thumbnail_base_url . ', пуст в записи' . $this->id );
			}
		}

		public function getThumbnailImage ()
		{

			return Html::img ( Yii::$app->glide->createSignedUrl ( [
				'glide/index' ,
				'path' => $this->thumbnail_path ,
				'w'    => 200 ,
			] , TRUE ) , [ 'class' => '' ] );
		}

		public function getMainImage ()
		{

			return Html::img ( Yii::$app->glide->createSignedUrl ( [
				'glide/index' ,
				'path' => $this->thumbnail_path ,
				'w'    => 800 ,
			] , TRUE ) , [ 'class' => '' ] );
		}

		public function getThumbnailSrc ()
		{

			return Yii::$app->glide->createSignedUrl ( [
				'glide/index' ,
				'path' => $this->thumbnail_path ,
				'w'    => 200 ,
			] , TRUE );
		}

		public function getMainSrc ()
		{

			return Yii::$app->glide->createSignedUrl ( [
				'glide/index' ,
				'path' => $this->thumbnail_path ,
				'w'    => 800 ,
			] , TRUE );
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getAttribute_id ()
		{

			if ( !empty( $this->attribute_id ) ) {
				return $this->attribute_id;
			}
			else {
				throw new UnknownPropertyException( $this->attribute_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getDescription ()
		{

			if ( !empty( $this->description ) ) {
				return $this->description;
			}
			else {
				throw new UnknownPropertyException( $this->description . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getAuthor_id ()
		{

			if ( !empty( $this->author_id ) ) {
				return $this->author_id;
			}
			else {
				throw new UnknownPropertyException( $this->author_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getUpdater_id ()
		{

			if ( !empty( $this->updater_id ) ) {
				return $this->updater_id;
			}
			else {
				throw new UnknownPropertyException( $this->updater_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getCreate_time ()
		{

			if ( !empty( $this->create_time ) ) {
				return $this->create_time;
			}
			else {
				throw new UnknownPropertyException( $this->create_time . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getUpdate_time ()
		{

			if ( !empty( $this->update_time ) ) {
				return $this->update_time;
			}
			else {
				throw new UnknownPropertyException( $this->update_time . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getStatus_id ()
		{

			if ( !empty( $this->status_id ) ) {
				return $this->status_id;
			}
			else {
				throw new UnknownPropertyException( $this->status_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * Relations
		 **/

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedCategories ()
		{

			return $this->hasOne ( Categories::className () , [ 'id' => 'category_id' ] );
		}

		public function getRelatedCompliments ()
		{

			return $this->hasMany ( Compliments::className () , [ 'id' => 'compliments_id' ] )->via ( 'viaCompliments' );
		}

		public function getViaCompliments ()
		{

			return $this->hasMany ( GoodsCompliments::className () , [ 'good_id' => 'id' ] );
		}

		public function getRelatedAttribute ()
		{

			return $this->hasMany ( Attributes::className () , [ 'id' => 'attribute_id' ] )->via ( 'viaAttribute' );
		}

		public function getViaAttribute ()
		{

			return $this->hasMany ( AttributesAndGoods::className () , [ 'good_id' => 'id' ] );
		}

		public function setRelatedAttribute ( $value )
		{

			return $this->relatedAttribute = $value;
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedCategory ()
		{

			return $this->hasOne ( Categories::className () , [ 'id' => 'category_id' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedId0 ()
		{

			return $this->hasOne ( OrderedGoods::className () , [ 'good_id' => 'id' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedStatus ()
		{

			return $this->hasOne ( Statuses::className () , [ 'id' => 'status_id' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedPrice ()
		{

			return $this->hasMany ( GoodsPriceType::className () , [ 'good_id' => 'id' ] );
		}

		public function setRelatedPrice ( $value )
		{

			return $this->relatedPrice = $value;
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedOrderedGoods ()
		{

			return $this->hasMany ( OrderedGoods::className () , [ 'good_id' => 'id' ] );
		}

		/**
		 * Admin form generator
		 **/
		public function getFormConfig ()
		{

			return $this->getFormConfigPizza ();
		}

		public function getFormConfigPizza ()
		{

			return [

				'label' => [
					'type' => ActiveFormBuilder::INPUT_TEXT ,
				] ,

				'category_id' => [
					'type'         => ActiveFormBuilder::INPUT_DROPDOWN_LIST ,
					'items'        => ArrayHelper::map ( Categories::find ()->all () , 'id' , 'label' ) ,
					'defaultValue' => 4 ,
					'options'      => [
						'prompt' => 'Выберите категорию' ,
					] ,
				] ,

				'relatedPrice' => [
					'type'        => ActiveFormBuilder::INPUT_WIDGET ,
					'widgetClass' => MultipleInput::className () ,
					'options'     => [
						'model'   => $this->getRelatedPrice () ,
						'columns' => [
							[
								'name'        => 'price' ,
								'enableError' => TRUE ,
								'title'       => 'Цена' ,
								'options'     => [
									'class'       => 'input-priority' ,
									'placeholder' => '500' ,
								] ,
							] ,
							[
								'name'    => 'dia' ,
								'type'    => 'dropDownList' ,
								'title'   => 'Диаметр' ,
								'items'   => [
									25 => '25' ,
									31 => '31' ,
									41 => '41' ,
								] ,
								'options' => [
									'prompt' => 'Выберите диаметр' ,
								] ,
							] ,
						] ,
					] ,
				] ,

				'thumbnail' => [
					'type'        => ActiveFormBuilder::INPUT_WIDGET ,
					'widgetClass' => Upload::className () ,
					'options'     => [
						'url'         => [ '/file-storage/upload' ] ,
						'maxFileSize' => 5000000 ,
						// 5 MiB
					] ,
				] ,

				'editableAttr' => [
					'type'  => ActiveFormBuilder::INPUT_CHECKBOX_LIST ,
					'items' => ArrayHelper::map ( Attributes::find ()->all () , 'id' , 'label' ) ,
				] ,

				'description' => [
					'type'        => ActiveFormBuilder::INPUT_WIDGET ,
					'widgetClass' => \yii\imperavi\Widget::className () ,
					'options'     => [
						'plugins' => [
							'fullscreen' ,
							'fontcolor' ,
							'video' ,
						] ,
						'options' => [
							'minHeight'       => 400 ,
							'maxHeight'       => 400 ,
							'buttonSource'    => TRUE ,
							'convertDivs'     => FALSE ,
							'removeEmptyTags' => FALSE ,
							'imageUpload'     => Yii::$app->urlManager->createUrl ( [ '/file-storage/upload-imperavi' ] ) ,
						] ,
					] ,
				] ,

				'status_id' => [
					'type'         => ActiveFormBuilder::INPUT_DROPDOWN_LIST ,
					'items'        => ArrayHelper::map ( Statuses::find ()->andFilterWhere ( [ 'class' => self::className () ] )->all () , 'id' , 'label' ) ,
					'defaultValue' => 3 ,
				] ,

			];
		}

		public function getPrice ()
		{

			return new GoodsPriceType();
		}

		public function fields ()
		{

			return [
				'id' ,
				'label' ,
				'thumbnail_base_url' ,
				'thumbnail_path' ,
				'description' ,
				'status_id' ,
			];
		}

		public function extraFields ()
		{

			return [
				'relatedCategories' ,
				'relatedAttribute' ,
				'relatedStatus' ,
				'relatedPrice' ,
				'relatedCompliments' ,
			];
		}

	}
