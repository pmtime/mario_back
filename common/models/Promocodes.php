<?php

	namespace common\models;

	use Yii;
	use yii\behaviors\BlameableBehavior;
	use yii\behaviors\TimestampBehavior;
	use \yii\base\UnknownPropertyException;
	use yii\db\Expression;
	use metalguardian\formBuilder\ActiveFormBuilder;
	use yii\helpers\ArrayHelper;
	use unclead\widgets\MultipleInput;
	use common\addons\behaviors\RelationHasManyBehavior;
	/**
	 * This is the model class for table "{{%promocodes}}".
	 *
	 * @property integer                   $id
	 * @property string                    $serial
	 * @property string                    $from
	 * @property string                    $to
	 * @property integer                   $sale
	 * @property integer                   $multiple
	 * @property integer                   $status_id
	 * @property integer                   $author_id
	 * @property integer                   $updater_id
	 * @property string                    $create_time
	 * @property integer                   $update_time
	 *
	 * @property Orders[]                  $orders
	 * @property GoodsComplimentsPromocode $id0
	 * @property Statuses                  $status
	 */
	class Promocodes extends \yii\db\ActiveRecord
	{
		public $count;
		/**
		 * @inheritdoc
		 */
		public static function tableName ()
		{

			return '{{%promocodes}}';
		}

		/**
		 * @inheritdoc
		 */
		public function behaviors ()
		{

			return [
				[
					'class'              => BlameableBehavior::className () ,
					'createdByAttribute' => 'author_id' ,
					'updatedByAttribute' => 'updater_id' ,
				] ,
				'relatedGoo'=> [
						'class'     => RelationHasManyBehavior::className () ,
						'relations' => [
							'relatedGoods' => 'relatedGoods' ,
						],
					] ,
				[
					'class'              => TimestampBehavior::className () ,
					'createdAtAttribute' => 'create_time' ,
					'updatedAtAttribute' => 'update_time' ,
					'value'              => new Expression( 'NOW()' ) ,
				] ,

			];

		}

		/**
		 * @inheritdoc
		 */
		public function rules ()
		{

			return [

				[
					[
						'from' ,
						'to',
					    'relatedGoods',
					    'count',
						'sale' ,

					] ,
					'safe',
				] ,
				[
					[
						'multiple' ,
						
					] ,
					'integer',
				] ,
				[
					[ 'status_id' ] ,
					'safe',
				] ,
				[
					[ 'serial' ] ,
					'string' ,
					'max' => 6,
				] ,
			];
		}

		/**
		 * @inheritdoc
		 */
		public function attributeLabels ()
		{

			return [
				'id'          => 'ID' ,
				'serial'      => 'Номер' ,
				'from'        => 'С даты' ,
				'to'          => 'По дату' ,
				'sale'        => 'Размер скидки' ,
				'multiple'    => 'Многоразовая?' ,
				'status_id'   => 'Статус' ,
				'author_id'   => 'Author ID' ,
				'updater_id'  => 'Updater ID' ,
				'create_time' => 'Create Time' ,
				'update_time' => 'Update Time' ,
				'relatedGoods' => 'Товары' ,
			];
		}

		/**
		 * @inheritdoc
		 * @return \common\models\query\PromocodesQuery the active query used by this AR class.
		 */
		public static function find ()
		{

			return new \common\models\query\PromocodesQuery( get_called_class () );
		}

		/**
		 * Access methods for the table attributes
		 **/
		public function getRelatedGoods ()
		{

			return $this->hasMany ( GoodsComplimentsPromocode::className () , [ 'promocode_id' => 'id' ] )->andFilterWhere(['class'=>'Goods']);
		}

		public function getViaGoods()
		{
			return $this->hasMany(GoodsComplimentsPromocode::className(), ['promocode_id' => 'id']);
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getGoods()
		{
			return $this->hasMany(Goods::className(), ['id' => 'class_id'])
				->via('viaGoods');
		}
		public function setRelatedGoods ( $value )
		{

			return $this->relatedGoods = $value;
		}


		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getId ()
		{

			if ( !empty( $this->id ) ) {
				return $this->id;
			}
			else {
				throw new UnknownPropertyException( $this->id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getSerial ()
		{

			if ( !empty( $this->serial ) ) {
				return $this->serial;
			}
			else {
				throw new UnknownPropertyException( $this->serial . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getFrom ()
		{

			if ( !empty( $this->from ) ) {
				return $this->from;
			}
			else {
				throw new UnknownPropertyException( $this->from . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getTo ()
		{

			if ( !empty( $this->to ) ) {
				return $this->to;
			}
			else {
				throw new UnknownPropertyException( $this->to . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getSale ()
		{

			if ( !empty( $this->sale ) ) {
				return $this->sale;
			}
			else {
				throw new UnknownPropertyException( $this->sale . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getMultiple ()
		{

			if ( !empty( $this->multiple ) ) {
				return $this->multiple;
			}
			else {
				throw new UnknownPropertyException( $this->multiple . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getStatus_id ()
		{

			if ( !empty( $this->status_id ) ) {
				return $this->status_id;
			}
			else {
				throw new UnknownPropertyException( $this->status_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getAuthor_id ()
		{

			if ( !empty( $this->author_id ) ) {
				return $this->author_id;
			}
			else {
				throw new UnknownPropertyException( $this->author_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getUpdater_id ()
		{

			if ( !empty( $this->updater_id ) ) {
				return $this->updater_id;
			}
			else {
				throw new UnknownPropertyException( $this->updater_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getCreate_time ()
		{

			if ( !empty( $this->create_time ) ) {
				return $this->create_time;
			}
			else {
				throw new UnknownPropertyException( $this->create_time . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getUpdate_time ()
		{

			if ( !empty( $this->update_time ) ) {
				return $this->update_time;
			}
			else {
				throw new UnknownPropertyException( $this->update_time . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * Relations
		 **/

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedOrders ()
		{

			return $this->hasMany ( Orders::className () , [ 'promocode_id' => 'id' ] );
		}

		public function getCountOfActivatedPromo(){
			$activates = [];
			foreach ($this->getRelatedOrders()->all() as $order){
				if($order->promocode_id == $this->id){
					$activates[] = $order->id;
				}
			}

			return count($activates);
		}
		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedId0 ()
		{

			return $this->hasOne ( GoodsComplimentsPromocode::className () , [ 'promocode_id' => 'id' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedStatus ()
		{

			return $this->hasOne ( Statuses::className () , [ 'id' => 'status_id' ] );
		}

		/**
		 * Admin form generator
		 **/
		public function getFormConfig ()
		{

			return [

				'serial' => [
					'type'    => ActiveFormBuilder::INPUT_TEXT ,
					'options' => [
						'value' => ($this->serial) ? $this->serial : substr ( md5 ( microtime () ) , rand ( 0 , 26 ) , 6 ) ,
					],
				] ,
				/*
				 * todo: Удалить 2amigos datepicker из composer.json
				 * */
				'from' => [
					'type'        => ActiveFormBuilder::INPUT_WIDGET ,
					'widgetClass' => \kartik\date\DatePicker::className() ,
					'options'     => [
						'value' => date('d.m.y'),
						'options' => [
							'pluginOptions' => [
								'format' => 'dd.mm.yyyy',
								'todayHighlight' => true
							]
						],

					],
				] ,

				'to' => [
					'type'        => ActiveFormBuilder::INPUT_WIDGET ,
					'widgetClass' => \kartik\date\DatePicker::className() ,
					'options'     => [
						'value' => date('d.m.y'),
						'options' => [
							'pluginOptions' => [
								'format' => 'dd.mm.yyyy',
								'todayHighlight' => true
							]
						],

					],
				] ,
				'relatedGoods' => [
					'type'        => ActiveFormBuilder::INPUT_WIDGET ,
					'widgetClass' => MultipleInput::className () ,
					'options'     => [
						'model'   => $this->getRelatedGoods () ,
						'limit'   => 4 ,
						'columns' => [
							[
								'title'  => 'Класс' ,
								'name' => 'class',
								'defaultValue'=>'Goods',
								'type'    => ActiveFormBuilder::INPUT_HIDDEN,

							] ,
							[
								'name'         => 'class_id' ,
								'type'         => 'dropDownList' ,
								'title'        => 'Выберите товар' ,
								'items' => ArrayHelper::map ( Goods::find ()->all () , 'id' , 'label' ) ,
								'options' => [
									'prompt' => 'Нет' ,
								] ,
							] ,
							[
								'name'         => 'count' ,
								'type'    => ActiveFormBuilder::INPUT_TEXT ,
								'title'        => 'Кол-во' ,
							] ,
						] ,
					],
				] ,
				'sale' => [
					'type' => ActiveFormBuilder::INPUT_TEXT ,
				] ,

				'multiple' => [
					'type' => ActiveFormBuilder::INPUT_CHECKBOX ,
				] ,
				'count' => [
					'type' => ActiveFormBuilder::INPUT_TEXT ,
				] ,
				'status_id' => [
					'type'    => ActiveFormBuilder::INPUT_DROPDOWN_LIST ,
					'items'   => ArrayHelper::map ( Statuses::find ()->andFilterWhere(['class'=>self::className()])->orderBy('id DESC')->all () , 'id' , 'label' ) ,
					'defaultValue'=>7,
				] ,

			];
		}
		public function fields()
		{
			return ['id', 'serial','from','to','sale','multiple','status_id'];
		}

		public function extraFields()
		{
			return ['relatedOrders','relatedStatus','relatedGoods','goods'];
		}
	}
