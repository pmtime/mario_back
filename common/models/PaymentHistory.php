<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%payment_history}}".
 *
 * @property integer $id
 * @property integer $summary
 * @property string $date
 * @property integer $order_id
 * @property integer $status
 */
class PaymentHistory extends \yii\db\ActiveRecord
{
                    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_history}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                                                                                                            
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['summary', 'order_id', 'status'], 'integer'],
            [['date'], 'safe'],
                                                                                                                            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'summary' => 'Summary',
            'date' => 'Date',
            'order_id' => 'Order ID',
            'status' => 'Status',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\PaymentHistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\PaymentHistoryQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){ 
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getSummary()
    {
        if(!empty($this->summary)){ 
            return $this->summary;
        }else{
            throw new UnknownPropertyException($this->summary.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getDate()
    {
        if(!empty($this->date)){ 
            return $this->date;
        }else{
            throw new UnknownPropertyException($this->date.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getOrder_id()
    {
        if(!empty($this->order_id)){ 
            return $this->order_id;
        }else{
            throw new UnknownPropertyException($this->order_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getStatus()
    {
        if(!empty($this->status)){ 
            return $this->status;
        }else{
            throw new UnknownPropertyException($this->status.', пуст в записи'. $this->id);
        }
    }
        
    /**
    * Relations
    **/
	public function fields()
	{
		return ['id', 'order_id','summary','status'];
	}
    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'summary' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'date' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],    
        
    'order_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'status' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
];}}
