<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%ordered_goods}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $good_id
 * @property integer $count
 */
class OrderedProducts extends \yii\db\ActiveRecord
{
                
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ordered_goods}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                                                                        
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['order_id', 'good_id'], 'required'],
            [['order_id', 'good_id', 'count'], 'integer'],
            [['order_id', 'good_id', 'count','price_id','thickness'], 'safe'],
                                                                                                        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'good_id' => 'Good ID',
            'count' => 'Count',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\OrderedProductsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\OrderedProductsQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){ 
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getOrder_id()
    {
        if(!empty($this->order_id)){ 
            return $this->order_id;
        }else{
            throw new UnknownPropertyException($this->order_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getGood_id()
    {
        if(!empty($this->good_id)){ 
            return $this->good_id;
        }else{
            throw new UnknownPropertyException($this->good_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getCount()
    {
        if(!empty($this->count)){ 
            return $this->count;
        }else{
            throw new UnknownPropertyException($this->count.', пуст в записи'. $this->id);
        }
    }
        
    /**
    * Relations
    **/

    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'order_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'good_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'count' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
];}}
