<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%sms_template}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $template
 */
class SmsTemplate extends \yii\db\ActiveRecord
{
            
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sms_template}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                                    
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['template'], 'string'],
            [['label'], 'string', 'max' => 255],
                                                                                    ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Название шаблона',
            'template' => 'Шаблон',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\SmsTemplateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\SmsTemplateQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){ 
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getLabel()
    {
        if(!empty($this->label)){ 
            return $this->label;
        }else{
            throw new UnknownPropertyException($this->label.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getTemplate()
    {
        if(!empty($this->template)){ 
            return $this->template;
        }else{
            throw new UnknownPropertyException($this->template.', пуст в записи'. $this->id);
        }
    }
        
    /**
    * Relations
    **/

    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'label' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],    
        
    'template' => [
        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
    ],    
];}}
