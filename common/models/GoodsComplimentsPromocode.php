<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%goods_compliments_promocode}}".
 *
 * @property integer $id
 * @property integer $promocode_id
 * @property string $class
 * @property integer $class_id
 *
 * @property Promocodes $promocodes
 */
class GoodsComplimentsPromocode extends \yii\db\ActiveRecord
{
                    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%goods_compliments_promocode}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                                                                                    
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['count','class','class_id','promocode_id'],'safe']
                                                                                                        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promocode_id' => 'Промокод',
            'class' => 'Кто',
            'class_id' => 'Связь',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\GoodsComplimentsPromocodeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\GoodsComplimentsPromocodeQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){ 
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getPromocode_id()
    {
        if(!empty($this->promocode_id)){ 
            return $this->promocode_id;
        }else{
            throw new UnknownPropertyException($this->promocode_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getClass()
    {
        if(!empty($this->class)){ 
            return $this->class;
        }else{
            throw new UnknownPropertyException($this->class.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getClass_id()
    {
        if(!empty($this->class_id)){ 
            return $this->class_id;
        }else{
            throw new UnknownPropertyException($this->class_id.', пуст в записи'. $this->id);
        }
    }
        
    /**
    * Relations
    **/
	public function getRelatedGood (  )
	{
		return $this->hasOne(Goods::className(),['id'=>'class_id']);

	}
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedPromocodes()
    {
        return $this->hasOne(Promocodes::className(), ['id' => 'promocode_id']);
    }

    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'promocode_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'class' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],    
        
    'class_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
];}

}
