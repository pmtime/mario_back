<?php

	namespace common\models;

	use Yii;
	use yii\behaviors\TimestampBehavior;
	use \yii\base\UnknownPropertyException;
	use yii\db\Expression;
	use metalguardian\formBuilder\ActiveFormBuilder;
	use yii\helpers\ArrayHelper;
	use unclead\widgets\MultipleInput;
	use common\addons\behaviors\RelationHasManyBehavior;

	/**
	 * This is the model class for table "{{%orders}}".
	 *
	 * @property integer        $id
	 * @property integer        $promocode_id
	 * @property integer        $customer_id
	 * @property integer        $delivery_id
	 * @property integer        $status_id
	 * @property integer        $payment_id
	 * @property string         $date
	 * @property string         $adres
	 * @property string         $comment
	 * @property integer        $summary
	 * @property string         $create_time
	 * @property string         $update_time
	 *
	 * @property OrderedGoods[] $orderedGoods
	 * @property Customers      $customer
	 * @property Deliveries     $delivery
	 * @property OrderedGoods   $id0
	 * @property Payment        $payment
	 * @property Promocodes     $promocode
	 * @property Statuses       $status
	 */
	class OrdersApi extends \yii\db\ActiveRecord
	{
		public $editableAttr = [];
		/**
		 * @inheritdoc
		 */
		public static function tableName ()
		{

			return '{{%orders}}';
		}

		/**
		 * @inheritdoc
		 */
		public function behaviors ()
		{

			return [
				[
					'class'              => TimestampBehavior::className () ,
					'createdAtAttribute' => 'create_time' ,
					'updatedAtAttribute' => 'update_time' ,
					'value'              => new Expression( 'NOW()' ) ,
				] ,


			];

		}

		/**
		 * @inheritdoc
		 */
		public function rules ()
		{

			return [

				[
					[
						'customer_id' ,
						'delivery_id' ,
						'status_id' ,
						'payment_type',
					] ,
					'required',
				] ,
				[
					[
						'promocode_id' ,
						'customer_id' ,
						'delivery_id' ,
						'status_id' ,
						'payment_id' ,
						'summary',
					] ,
					'integer',
				] ,
				[
					[
						'date' ,
						'relatedGoods',
					    'payment_id'
					] ,
					'safe',
				] ,
				[
					[ 'comment' ] ,
					'string',
				] ,
				[
					[ 'adres' ] ,
					'string' ,
					'max' => 255,
				] ,
				[
					[ 'customer_id' ] ,
					'exist' ,
					'skipOnError'     => TRUE ,
					'targetClass'     => Customers::className () ,
					'targetAttribute' => [ 'customer_id' => 'id' ],
				] ,
				[
					[ 'delivery_id' ] ,
					'exist' ,
					'skipOnError'     => TRUE ,
					'targetClass'     => Deliveries::className () ,
					'targetAttribute' => [ 'delivery_id' => 'id' ],
				] ,

				[
					[ 'promocode_id' ] ,
					'exist' ,
					'skipOnError'     => TRUE ,
					'targetClass'     => Promocodes::className () ,
					'targetAttribute' => [ 'promocode_id' => 'id' ],
				] ,
				[
					[ 'status_id' ] ,
					'exist' ,
					'skipOnError'     => TRUE ,
					'targetClass'     => Statuses::className () ,
					'targetAttribute' => [ 'status_id' => 'id' ],
				] ,
			];
		}

		/**
		 * @inheritdoc
		 */
		public function attributeLabels ()
		{

			return [
				'id'           => 'ID' ,
				'promocode_id' => 'Промокод' ,
				'customer_id'  => 'Клиент' ,
				'delivery_id'  => 'Доставка' ,
				'status_id'    => 'Статус' ,
				'payment_id'   => 'Метод оплаты' ,
				'date'         => 'Дата' ,
				'adres'        => 'Адрес' ,
				'comment'      => 'Комментарий' ,
				'summary'      => 'Сумма' ,
				'create_time'  => 'Время заказа' ,
				'update_time'  => 'Update Time' ,
			];
		}

		/**
		 * @inheritdoc
		 * @return \common\models\query\OrdersQuery the active query used by this AR class.
		 */
		public static function find ()
		{

			return new \common\models\query\OrdersQuery( get_called_class () );
		}

		/**
		 * Access methods for the table attributes
		 **/

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getId ()
		{

			if ( !empty( $this->id ) ) {
				return $this->id;
			}
			else {
				throw new UnknownPropertyException( $this->id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getPromocode_id ()
		{

			if ( !empty( $this->promocode_id ) ) {
				return $this->promocode_id;
			}
			else {
				throw new UnknownPropertyException( $this->promocode_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getCustomer_id ()
		{

			if ( !empty( $this->customer_id ) ) {
				return $this->customer_id;
			}
			else {
				throw new UnknownPropertyException( $this->customer_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getDelivery_id ()
		{

			if ( !empty( $this->delivery_id ) ) {
				return $this->delivery_id;
			}
			else {
				throw new UnknownPropertyException( $this->delivery_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getStatus_id ()
		{

			if ( !empty( $this->status_id ) ) {
				return $this->status_id;
			}
			else {
				throw new UnknownPropertyException( $this->status_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getPayment_id ()
		{

			if ( !empty( $this->payment_id ) ) {
				return $this->payment_id;
			}
			else {
				throw new UnknownPropertyException( $this->payment_id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getDate ()
		{

			if ( !empty( $this->date ) ) {
				return $this->date;
			}
			else {
				throw new UnknownPropertyException( $this->date . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getAdres ()
		{

			if ( !empty( $this->adres ) ) {
				return $this->adres;
			}
			else {
				throw new UnknownPropertyException( $this->adres . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getComment ()
		{

			if ( !empty( $this->comment ) ) {
				return $this->comment;
			}
			else {
				throw new UnknownPropertyException( $this->comment . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getSummary ()
		{

			if ( !empty( $this->summary ) ) {
				return $this->summary;
			}
			else {
				throw new UnknownPropertyException( $this->summary . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getCreate_time ()
		{

			if ( !empty( $this->create_time ) ) {
				return $this->create_time;
			}
			else {
				throw new UnknownPropertyException( $this->create_time . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getUpdate_time ()
		{

			if ( !empty( $this->update_time ) ) {
				return $this->update_time;
			}
			else {
				throw new UnknownPropertyException( $this->update_time . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * Relations
		 **/

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedGoods ()
		{
			return $this->hasMany ( OrderedGoods::className() , [ 'order_id' => 'id' ] );
		}

		public function setRelatedGoods($value)
		{
			return $this->relatedGoods = $value;
		}

		public function getRelatedViaGoods ()
		{

			return $this->hasMany ( Goods::className () , [ 'id' => 'good_id' ] )->viaTable ( OrderedGoods::tableName () , [ 'order_id' => 'id' ] );
		}



		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getViaGoods()
		{
			return $this->hasMany(OrderedGoods::className(), ['order_id' => 'id']);
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getGoods()
		{
			return $this->hasMany(Goods::className(), ['id' => 'good_id'])
				->via('viaGoods');
		}


		public function getRelatedOrderedGoods(){
			return $this->hasMany(Goods::className(), ['id' => 'good_id'])
				->via('viaGoods');
		}
		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedCustomer ()
		{

			return $this->hasOne ( Customers::className () , [ 'id' => 'customer_id' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedDelivery ()
		{

			return $this->hasOne ( Deliveries::className () , [ 'id' => 'delivery_id' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedId0 ()
		{

			return $this->hasOne ( OrderedGoods::className () , [ 'order_id' => 'id' ] );
		}


		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedPayment ()
		{

			return $this->hasOne ( Payment::className () , [ 'id' => 'payment_id' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedPromocode ()
		{

			return $this->hasOne ( Promocodes::className () , [ 'id' => 'promocode_id' ] );
		}

		/**
		 * @return \yii\db\ActiveQuery
		 */
		public function getRelatedStatus ()
		{

			return $this->hasOne ( Statuses::className () , [ 'id' => 'status_id' ] );
		}

		/**
		 * Admin form generator
		 **/
		public function getFormConfig ()
		{

			return [
				'relatedGoods' => [
					'type'        => ActiveFormBuilder::INPUT_WIDGET ,
					'widgetClass' => MultipleInput::className () ,
					'model' => $this->getRelatedGoods(),
					'options'     => [
						'columns' => [
							[
								'name'  => 'good_id' ,
								'type'  => 'dropDownList' ,
								'title' => 'Товары' ,
								'items' => ArrayHelper::map ( Goods::find ()->all () , 'id' , 'label' ),
							] ,
							[
								'name'  => 'count' ,
								'title' => 'кол-во' ,
							] ,
						] ,
					],
				] ,

				'promocode_id' => [
					'type'    => ActiveFormBuilder::INPUT_DROPDOWN_LIST ,
					'items'   => ArrayHelper::map ( Promocodes::find ()->all () , 'id' , 'serial' ) ,
					'options' => [
						'prompt' => 'Выберите промокод' ,
					] ,
				] ,

				'customer_id' => [
					'type'    => ActiveFormBuilder::INPUT_DROPDOWN_LIST ,
					'items'   => ArrayHelper::map ( Customers::find ()->all () , 'id' , 'fullName' ) ,
					'options' => [
						'prompt' => 'Выберите клиента' ,
					] ,
				] ,

				'delivery_id' => [
					'type'    => ActiveFormBuilder::INPUT_DROPDOWN_LIST ,
					'items'   => ArrayHelper::map ( Deliveries::find ()->all () , 'id' , 'label' ) ,
					'options' => [
						'prompt' => 'Выберите тип доставки' ,
					] ,
				] ,

				'status_id' => [
					'type'    => ActiveFormBuilder::INPUT_DROPDOWN_LIST ,
					'items'   => ArrayHelper::map ( Statuses::find ()->all () , 'id' , 'label' ) ,
					'options' => [
						'prompt' => 'Выберите статус' ,
					] ,
				] ,

				'payment_id' => [
					'type'    => ActiveFormBuilder::INPUT_DROPDOWN_LIST ,
					'items'   => ArrayHelper::map ( Payment::find ()->all () , 'id' , 'summary' ) ,
					'options' => [
						'prompt' => 'Выберите запись о оплате' ,
					] ,
				] ,

				'date' => [
					'type' => ActiveFormBuilder::INPUT_TEXT ,
				] ,

				'adres' => [
					'type' => ActiveFormBuilder::INPUT_TEXT ,
				] ,

				'comment' => [
					'type' => ActiveFormBuilder::INPUT_TEXT ,
				] ,

				'summary' => [
					'type' => ActiveFormBuilder::INPUT_TEXT ,
				] ,

			];
		}

		public function fields()
		{
			return ['id', 'adres','comment','summary'];
		}

		public function extraFields()
		{
			return ['relatedOrderedGoods','relatedCustomer','relatedDelivery','relatedPayment','relatedPromocode','viaGoods'];
		}
	}
