<?php

namespace common\models;

use Yii;
use \yii\base\UnknownPropertyException;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
use unclead\widgets\MultipleInput;
use common\addons\behaviors\RelationHasManyBehavior;
/**
 * This is the model class for table "{{%sms_senders}}".
 *
 * @property integer $id
 * @property string $date_from
 * @property string $date_to
 * @property string $sum
 * @property integer $good_id
 */
class SmsSoloCustomer extends Sms
{

    public function behaviors()
    {
    return [
	    [
		    'class'=>RelationHasManyBehavior::className(),
		    'relations'=>[
			    'relatedCustomers'=>'relatedCustomers',
		    ]
	    ],
    ];

    }




	public function getRelatedCustomers ()
	{
		return $this->hasMany ( SmsCustomers::className() , [ 'sms_id' => 'id' ] );
	}

	public function setRelatedCustomers($value)
	{
		return $this->relatedCustomers = $value;
	}

	public function getRelatedCategories ()
	{
		return $this->hasMany ( SmsCategories::className() , [ 'sms_id' => 'id' ] );
	}

	public function setRelatedCategories($value)
	{
		return $this->relatedCategories = $value;
	}

	public function getRecepients(){

		return $this->hasMany(SmsRecepients::className(),['sms_id'=>'id']);

	}

	public function getRecepientsCount(){

		return $this->hasMany(SmsRecepients::className(),['sms_id'=>'id'])->count();

	}

	public function getRecepientsCountSended(){

		return $this->hasMany(SmsRecepients::className(),['sms_id'=>'id'])->andFilterWhere(['like','status',1])->count();

	}

	public function getTemplate(){
		return $this->hasOne(SmsTemplate::className(),['id'=>'template_id']);
	}
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Название рассылки',
            'template_id' => 'Шаблон сообщения',
            'relatedGoods' => 'Товары',
            'relatedDates' => 'Даты',
            'relatedSummaries' => 'Итого заказа',
            'recepientscount' => 'Кол-во получателей',
            'recepientscountsended' => 'Кол-во доставленных',
            'relatedCustomers' => 'Клиенты',
            'relatedCategories' => 'Категории',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\SmsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\SmsQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/


    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }







    /**
    * Relations
    **/
	public function afterSave($insert, $changedAttributes)
	{

		parent::afterSave ( $insert , $changedAttributes );

		$customers = Orders::find ()->joinWith ( [ 'relatedCustomers c' ] );
		/*
		 * Add summary constructor rule
		 *  */
		if ( count ( $this->getRelatedCustomers()->asArray ()->all () ) > 0 ) {

			foreach ( $this->getRelatedCustomers ()->all () as $dateRule ) {
				$customers->andFilterWhere ( [
					'like' ,
					'c.id' ,
					$dateRule->customer_id ,
				] );
				}

		}
		/*
		 * Add dates BETWEEN filter
		 *  */
		/*
		 * Add Goods like filter
		 *  */


		/*
		 * Do clients array and send SMS
		 *  */
		if ( !empty( $customers->asArray ()->all () ) ) {
			foreach ( $customers->asArray ()->all () as $customer ) {
				$check = SmsRecepients::find ()->andFilterWhere ( [
						'like' ,
						'sms_id' ,
						$this->id
					] )->andFilterWhere ( [
						'like' ,
						'customer_id' ,
						$customer[ 'customer_id' ]
					] );
				if ( !$check->one () ) {
					$addRecepient = new SmsRecepients();
					$addRecepient->sms_id = $this->id;
					$addRecepient->customer_id = $customer[ 'customer_id' ];
					$addRecepient->status = 0;
					$addRecepient->save ();
				}
			}

		}

	}
    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
	'label' => [
		'type' => ActiveFormBuilder::INPUT_TEXT,
	],

	'template_id' => [
		'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
		'items' => ArrayHelper::map(SmsTemplate::find()->all(),'id','label'),
		'options' => [
			'prompt' => 'Выберите шаблон',
		],
	],
	'relatedCustomers'=>[
		'type'        => ActiveFormBuilder::INPUT_WIDGET ,
		'widgetClass' => MultipleInput::className () ,
		'model' => $this->getRelatedCustomers(),
		'options'     => [
			'columns' => [
				[
					'name'  => 'customer_id' ,
					'type'  => 'dropDownList' ,
					'title' => 'Клиент' ,
					'items' => ArrayHelper::map ( Customers::find ()->all () , 'id' , 'fullName' ),
					'options' => [
						'prompt' => 'Выберите клиента',
					],
				] ,
			] ,
		],
	] ,
];}}
