<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%goods_compliments}}".
 *
 * @property integer $id
 * @property integer $good_id
 * @property integer $compliments_id
 * @property integer $count
 */
class GoodsCompliments extends \yii\db\ActiveRecord
{
                
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%goods_compliments}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                                                                        
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['good_id', 'compliments_id', 'count','order_id'], 'integer'],
            [['good_id', 'compliments_id', 'count','order_id'], 'safe'],
                                                                                                        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'good_id' => 'Good ID',
            'compliments_id' => 'Compliments ID',
            'count' => 'Count',
        ];
    }

	
    /**
     * @inheritdoc
     * @return \common\models\query\GoodsComplimentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\GoodsComplimentsQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){ 
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getGood_id()
    {
        if(!empty($this->good_id)){ 
            return $this->good_id;
        }else{
            throw new UnknownPropertyException($this->good_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getCompliments_id()
    {
        if(!empty($this->compliments_id)){ 
            return $this->compliments_id;
        }else{
            throw new UnknownPropertyException($this->compliments_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getCount()
    {
        if(!empty($this->count)){ 
            return $this->count;
        }else{
            throw new UnknownPropertyException($this->count.', пуст в записи'. $this->id);
        }
    }
        
    /**
    * Relations
    **/
	public function getRelatedCompliment (  )
	{

		return $this->hasOne(Compliments::className(),['id'=>'compliments_id']);
	}
    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'good_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'compliments_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'count' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
];}}
