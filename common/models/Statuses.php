<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%statuses}}".
 *
 * @property integer $id
 * @property string $class
 * @property string $label
 * @property string $color
 * @property integer $author_id
 * @property integer $updater_id
 *
 * @property Compliments[] $compliments
 * @property Goods[] $goods
 * @property Orders[] $orders
 * @property Promocodes[] $promocodes
 * @property Vacancies[] $vacancies
 */
class Statuses extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%statuses}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [
            [
             'class' => BlameableBehavior::className(),
             'createdByAttribute' => 'author_id',
             'updatedByAttribute' => 'updater_id',
            ],

    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['class', 'label', 'color'], 'string', 'max' => 255],
                                                                                                                                                ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'class' => 'Кто?',
            'label' => 'Название',
            'color' => 'Цвет',
            'author_id' => 'Author ID',
            'updater_id' => 'Updater ID',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\StatusesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\StatusesQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/


    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }

    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getClass()
    {
        if(!empty($this->class)){
            return $this->class;
        }else{
            throw new UnknownPropertyException($this->class.', пуст в записи'. $this->id);
        }
    }

    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getLabel()
    {
        if(!empty($this->label)){
            return $this->label;
        }else{
            throw new UnknownPropertyException($this->label.', пуст в записи'. $this->id);
        }
    }

    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getColor()
    {
        if(!empty($this->color)){
            return $this->color;
        }else{
            throw new UnknownPropertyException($this->color.', пуст в записи'. $this->id);
        }
    }

    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getAuthor_id()
    {
        if(!empty($this->author_id)){
            return $this->author_id;
        }else{
            throw new UnknownPropertyException($this->author_id.', пуст в записи'. $this->id);
        }
    }

    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getUpdater_id()
    {
        if(!empty($this->updater_id)){
            return $this->updater_id;
        }else{
            throw new UnknownPropertyException($this->updater_id.', пуст в записи'. $this->id);
        }
    }

    /**
    * Relations
    **/

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedCompliments()
    {
    return $this->hasMany(Compliments::className(), ['status_id' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedGoods()
    {
    return $this->hasMany(Goods::className(), ['status_id' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedOrders()
    {
    return $this->hasMany(Orders::className(), ['status_id' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedPromocodes()
    {
    return $this->hasMany(Promocodes::className(), ['status_id' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedVacancies()
    {
    return $this->hasMany(Vacancies::className(), ['status_id' => 'id']);
    }
    public static function createClassArray(){
        return [
            Attributes::className()=>'Атрибуты',
            Callback::className()=>'Обратный звонок',
            Categories::className()=>      'Категории',
            Compliments::className()=>  'Комплименторы',
	        Customers::className()=>        'Клиенты',
            Deliveries::className()=>       'Доставка',
            Goods::className()=>         'Товары',
            Orders::className()=>         'Заказы',
            Payment::className()=>         'Оплата',
            Promocodes::className()=>      'Промокоды',
            Vacancies::className()=>       'Вакансии',
        ];
    }
    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [

    'class' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => self::createClassArray(),
        'options' => [
            'prompt' => 'Выберите к кому относится статус',
        ],
    ],


    'label' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],

    'color' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],


];}}
