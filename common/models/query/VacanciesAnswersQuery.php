<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\VacanciesAnswers]].
 *
 * @see \common\models\VacanciesAnswers
 */
class VacanciesAnswersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status_id]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\VacanciesAnswers[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\VacanciesAnswers|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
