<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Goods]].
 *
 * @see \common\models\Goods
 */
class GoodsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status_id]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\Goods[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Goods|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
