<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\SmsCustomers]].
 *
 * @see \common\models\SmsCustomers
 */
class SmsCustomersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status_id]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\SmsCustomers[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\SmsCustomers|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
