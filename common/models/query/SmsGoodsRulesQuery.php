<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\SmsGoodsRules]].
 *
 * @see \common\models\SmsGoodsRules
 */
class SmsGoodsRulesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status_id]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\SmsGoodsRules[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\SmsGoodsRules|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
