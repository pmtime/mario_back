<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\PaymentHistory]].
 *
 * @see \common\models\PaymentHistory
 */
class PaymentHistoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status_id]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\PaymentHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\PaymentHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
