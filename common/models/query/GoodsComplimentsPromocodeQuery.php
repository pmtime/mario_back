<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\GoodsComplimentsPromocode]].
 *
 * @see \common\models\GoodsComplimentsPromocode
 */
class GoodsComplimentsPromocodeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status_id]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\GoodsComplimentsPromocode[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\GoodsComplimentsPromocode|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
