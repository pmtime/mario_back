<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Callback]].
 *
 * @see \common\models\Callback
 */
class CallbackQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status_id]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\Callback[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Callback|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
