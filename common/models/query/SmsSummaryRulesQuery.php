<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\SmsSummaryRules]].
 *
 * @see \common\models\SmsSummaryRules
 */
class SmsSummaryRulesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status_id]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\SmsSummaryRules[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\SmsSummaryRules|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
