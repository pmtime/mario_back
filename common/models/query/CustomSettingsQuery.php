<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\CustomSettings]].
 *
 * @see \common\models\CustomSettings
 */
class CustomSettingsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status_id]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\CustomSettings[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\CustomSettings|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
