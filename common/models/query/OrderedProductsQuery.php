<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\OrderedProducts]].
 *
 * @see \common\models\OrderedProducts
 */
class OrderedProductsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status_id]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\OrderedProducts[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\OrderedProducts|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
