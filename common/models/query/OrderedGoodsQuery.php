<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\OrderedGoods]].
 *
 * @see \common\models\OrderedGoods
 */
class OrderedGoodsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status_id]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\OrderedGoods[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\OrderedGoods|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
