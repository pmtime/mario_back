<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Promocodes]].
 *
 * @see \common\models\Promocodes
 */
class PromocodesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status_id]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\Promocodes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Promocodes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
