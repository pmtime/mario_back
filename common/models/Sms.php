<?php

	namespace common\models;

	use Yii;
	use \yii\base\UnknownPropertyException;
	use metalguardian\formBuilder\ActiveFormBuilder;
	use yii\helpers\ArrayHelper;
	use unclead\widgets\MultipleInput;
	use common\addons\behaviors\RelationHasManyBehavior;

	/**
	 * This is the model class for table "{{%sms_senders}}".
	 *
	 * @property integer $id
	 * @property string  $date_from
	 * @property string  $date_to
	 * @property string  $sum
	 * @property integer $good_id
	 */
	class Sms extends \yii\db\ActiveRecord
	{

		/**
		 * @inheritdoc
		 */
		public static function tableName ()
		{

			return '{{%sms_senders}}';
		}

		/**
		 * @inheritdoc
		 */
		public function behaviors ()
		{

			return [
				[
					'class'     => RelationHasManyBehavior::className () ,
					'relations' => [
						'relatedCustomers'  => 'relatedCustomers' ,
						'relatedGoods'      => 'relatedGoods' ,
						'relatedSummaries'  => 'relatedSummaries' ,
						'relatedDates'      => 'relatedDates' ,
						'relatedCategories' => 'relatedCategories' ,
					],
				] ,
			];

		}

		/**
		 * @inheritdoc
		 */
		public function rules ()
		{

			return [

				[
					[
						'relatedGoods' ,
						'relatedDates' ,
						'relatedSummaries' ,
						'relatedCustomers' ,
						'relatedCategories' ,
						'label' ,
						'template_id' ,
					] ,
					'safe',
				] ,
			];
		}

		public function getRelatedGoods ()
		{

			return $this->hasMany ( SmsGoodsRules::className () , [ 'sms_id' => 'id' ] );
		}

		public function setRelatedGoods ( $value )
		{

			return $this->relatedGoods = $value;
		}

		public function getRelatedDates ()
		{

			return $this->hasMany ( SmsDateRules::className () , [ 'sms_id' => 'id' ] );
		}

		public function setRelatedDates ( $value )
		{

			return $this->relatedDates = $value;
		}

		public function getRelatedSummaries ()
		{

			return $this->hasMany ( SmsSummaryRules::className () , [ 'sms_id' => 'id' ] );
		}

		public function setRelatedSummaries ( $value )
		{

			return $this->relatedSummaries = $value;
		}

		public function getRelatedCustomers ()
		{

			return $this->hasMany ( SmsCustomers::className () , [ 'sms_id' => 'id' ] );
		}

		public function setRelatedCustomers ( $value )
		{

			return $this->relatedCustomers = $value;
		}

		public function getRelatedCategories ()
		{

			return $this->hasMany ( SmsCategories::className () , [ 'sms_id' => 'id' ] );
		}

		public function setRelatedCategories ( $value )
		{

			return $this->relatedCategories = $value;
		}

		public function getRecepients ()
		{

			return $this->hasMany ( SmsRecepients::className () , [ 'sms_id' => 'id' ] );

		}

		public function getRecepientsCount ()
		{

			return $this->hasMany ( SmsRecepients::className () , [ 'sms_id' => 'id' ] )->count ();

		}

		public function getRecepientsCountSended ()
		{

			return $this->hasMany ( SmsRecepients::className () , [ 'sms_id' => 'id' ] )->andFilterWhere ( [
				'like' ,
				'status' ,
				1,
			] )->count ();

		}

		public function getTemplate ()
		{

			return $this->hasOne ( SmsTemplate::className () , [ 'id' => 'template_id' ] );
		}

		/**
		 * @inheritdoc
		 */
		public function attributeLabels ()
		{

			return [
				'id'                    => 'ID' ,
				'label'                 => 'Название рассылки' ,
				'template_id'           => 'Шаблон сообщения' ,
				'relatedGoods'          => 'Товары' ,
				'relatedDates'          => 'Даты' ,
				'relatedSummaries'      => 'Итого заказа' ,
				'recepientscount'       => 'Кол-во получателей' ,
				'recepientscountsended' => 'Кол-во доставленных' ,
				'relatedCustomers'      => 'Клиенты' ,
				'relatedCategories'     => 'Категории' ,
			];
		}

		/**
		 * @inheritdoc
		 * @return \common\models\query\SmsQuery the active query used by this AR class.
		 */
		public static function find ()
		{

			return new \common\models\query\SmsQuery( get_called_class () );
		}

		/**
		 * Access methods for the table attributes
		 **/

		/**
		 * @return string
		 * @throws \yii\base\UnknownPropertyException;
		 */
		public function getId ()
		{

			if ( !empty( $this->id ) ) {
				return $this->id;
			}
			else {
				throw new UnknownPropertyException( $this->id . ', пуст в записи' . $this->id );
			}
		}

		/**
		 * Relations
		 **/
		public function afterSave ( $insert , $changedAttributes )
		{

			parent::afterSave ( $insert , $changedAttributes );

			$customers = Orders::find ()->joinWith ( [ 'relatedGoods g' ] );
			/*
			 * Add dates BETWEEN filter
			 *  */
			if ( count ( $this->getRelatedDates ()->asArray ()->all () ) > 0 ) {

				foreach ( $this->getRelatedDates ()->all () as $dateRule ) {

					switch ( $dateRule->getType () ) {

						case 1:
							$customers->andFilterWhere ( [
								'BETWEEN' ,
								'create_time' ,
								$dateRule->getDate () ,
								$dateRule->getDate_to () ,
							] );
							break;

						case 2:
							$customers->orFilterWhere ( [
								'BETWEEN' ,
								'create_time' ,
								$dateRule->getDate () ,
								$dateRule->getDate_to () ,
							] );
							break;

					}

				}

			}
			/*
			 * Add Goods like filter
			 *  */
			if ( count ( $this->getRelatedGoods ()->asArray ()->all () ) > 0 ) {

				foreach ( $this->getRelatedGoods ()->all () as $dateRule ) {

					switch ( $dateRule->getType () ) {

						case 1:
							$customers->andFilterWhere ( [
								'like' ,
								'g.id' ,
								$dateRule->good_id ,
							] );
							break;

						case 2:
							$customers->orFilterWhere ( [
								'like' ,
								'g.id' ,
								$dateRule->good_id ,
							] );
							break;

					}

				}

			}

			/*
			 * Add summary constructor rule
			 *  */
			if ( count ( $this->getRelatedSummaries ()->asArray ()->all () ) > 0 ) {

				foreach ( $this->getRelatedSummaries ()->all () as $dateRule ) {

					switch ( $dateRule->getType () ) {

						case 1:
							$customers->andWhere ( 'summary ' . $dateRule->compare_type . ' ' . $dateRule->summary );
							break;

						case 2:
							$customers->orWhere ( 'summary ' . $dateRule->compare_type . ' ' . $dateRule->summary );
							break;

					}

				}

			}
			/*
			 * Do clients array and send SMS
			 *  */
			if ( !empty( $customers->asArray ()->all () ) ) {
				$test = [];
				foreach ( $customers->asArray ()->all () as $customer ) {

					$check = SmsRecepients::find ()->andFilterWhere ( [
						'sms_id' => $this->id,
						'customer_id' => $customer[ 'customer_id' ],

					] );
					if ( is_null($check->one ()) ) {
							$addRecepient = new SmsRecepients();
							$addRecepient->sms_id = $this->id;
							$addRecepient->customer_id = $customer[ 'customer_id' ];
							$addRecepient->status = 0;
							$addRecepient->save ();
					}
				}


			}
			if ( !empty( $this->getRelatedCustomers ()->all () ) ) {
				foreach ( $this->getRelatedCustomers ()->all () as $rcustomer ) {
					if($rcustomer->customer_id != 0){
						$check = SmsRecepients::find ()->andFilterWhere ( [
							'like' ,
							'sms_id' ,
							$this->id,
						] )->andFilterWhere ( [
							'like' ,
							'customer_id' ,
							$rcustomer->customer_id,
						] );
						if ( !$check->one () ) {
							$addRecepient = new SmsRecepients();
							$addRecepient->sms_id = $this->id;
							$addRecepient->customer_id = $rcustomer->customer_id;
							$addRecepient->status = 0;
							$addRecepient->save ();
						}
					}

				}

			}
		}

		/**
		 * Admin form generator
		 **/
		public function getFormConfig ()
		{

			return [
				'label' => [
					'type' => ActiveFormBuilder::INPUT_TEXT ,
				] ,

				'template_id'       => [
					'type'    => ActiveFormBuilder::INPUT_DROPDOWN_LIST ,
					'items'   => ArrayHelper::map ( SmsTemplate::find ()->all () , 'id' , 'label' ) ,
					'options' => [
						'prompt' => 'Выберите шаблон' ,
					] ,
				] ,
				'relatedCustomers'  => [
					'type'        => ActiveFormBuilder::INPUT_WIDGET ,
					'widgetClass' => MultipleInput::className () ,
					'model'       => $this->getRelatedCustomers () ,
					'options'     => [
						'columns' => [
							[
								'name'    => 'customer_id' ,
								'type'    => 'dropDownList' ,
								'title'   => 'Клиент' ,
								'items'   => ArrayHelper::map ( Customers::find ()->all () , 'id' , 'fullName' ) ,
								'options' => [
									'prompt' => 'Выберите клиента' ,
								] ,
							] ,
						] ,
					] ,
				] ,
				'relatedCategories' => [
					'type'        => ActiveFormBuilder::INPUT_WIDGET ,
					'widgetClass' => MultipleInput::className () ,
					'model'       => $this->getRelatedCategories () ,
					'options'     => [
						'columns' => [
							[
								'name'    => 'category_id' ,
								'type'    => 'dropDownList' ,
								'title'   => 'Категория' ,
								'items'   => ArrayHelper::map ( Categories::find ()->all () , 'id' , 'label' ) ,
								'options' => [
									'prompt' => 'Выберите категорию' ,
								] ,
							] ,
						] ,
					] ,
				] ,
				'relatedGoods'      => [
					'type'        => ActiveFormBuilder::INPUT_WIDGET ,
					'widgetClass' => MultipleInput::className () ,
					'model'       => $this->getRelatedGoods () ,
					'options'     => [
						'columns' => [
							[
								'name'    => 'good_id' ,
								'type'    => 'dropDownList' ,
								'title'   => 'Товар' ,
								'items'   => ArrayHelper::map ( Goods::find ()->all () , 'id' , 'label' ) ,
								'options' => [
									'prompt' => 'Выберите товар' ,
								] ,

							] ,

							[
								'name'    => 'type' ,
								'type'    => 'dropDownList' ,
								'title'   => 'Тип правила' ,
								'items'   => [
									'1' => 'И' ,
									'2' => 'ИЛИ',
								] ,
								'options' => [
									'prompt' => 'Выберите правило' ,
								] ,

							] ,
						] ,
					] ,
				] ,
				'relatedDates'      => [
					'type'        => ActiveFormBuilder::INPUT_WIDGET ,
					'widgetClass' => MultipleInput::className () ,
					'model'       => $this->getRelatedDates () ,
					'options'     => [
						'columns' => [
							[
								'name'          => 'date' ,
								'type'          => \kartik\date\DatePicker::className () ,
								'title'         => 'С Даты' ,
								'value'         => date ( 'd.m.y' ) ,
								'items'         => [
									'0' => 'Saturday' ,
									'1' => 'Monday',
								] ,
								'options'       => [
									'pluginOptions' => [
										'format'         => 'dd.mm.yyyy' ,
										'todayHighlight' => TRUE,
									],
								] ,
								'headerOptions' => [
									'style' => 'width: 350px;' ,
									'class' => 'day-css-class',
								],
							] ,
							[
								'name'          => 'date_to' ,
								'type'          => \kartik\date\DatePicker::className () ,
								'title'         => 'По Дату' ,
								'value'         => date ( 'd.m.y' ) ,
								'items'         => [
									'0' => 'Saturday' ,
									'1' => 'Monday',
								] ,
								'options'       => [
									'pluginOptions' => [
										'format'         => 'dd.mm.yyyy' ,
										'todayHighlight' => TRUE,
									],
								] ,
								'headerOptions' => [
									'style' => 'width: 350px;' ,
									'class' => 'day-css-class',
								],
							] ,
							[
								'name'    => 'type' ,
								'type'    => 'dropDownList' ,
								'title'   => 'Тип правила' ,
								'items'   => [
									'1' => 'И' ,
									'2' => 'ИЛИ',
								] ,
								'options' => [
									'prompt' => 'Выберите правило' ,
								] ,
							] ,
						] ,
					] ,
				] ,
				'relatedSummaries'  => [
					'type'        => ActiveFormBuilder::INPUT_WIDGET ,
					'widgetClass' => MultipleInput::className () ,
					'model'       => $this->getRelatedSummaries () ,
					'options'     => [
						'columns' => [
							[
								'name'    => 'compare_type' ,
								'type'    => 'dropDownList' ,
								'title'   => 'Тип сравнения' ,
								'items'   => [
									'='  => 'Равно' ,
									'!=' => 'Не равно' ,
									'<'  => 'Меньше' ,
									'>'  => 'Больше' ,
									'>=' => 'Больше или равно' ,
								] ,
								'options' => [
									'prompt' => 'Выберите правило' ,
								] ,
							] ,
							[
								'name'  => 'summary' ,
								'title' => 'Правило' ,
							] ,
							[
								'name'    => 'type' ,
								'type'    => 'dropDownList' ,
								'title'   => 'Тип' ,
								'items'   => [
									'1' => 'И' ,
									'2' => 'ИЛИ',
								] ,
								'options' => [
									'prompt' => 'Выберите правило' ,
								] ,
							] ,
						] ,
					] ,
				] ,

			];
		}
	}
