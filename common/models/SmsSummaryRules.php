<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%sms_summary_rules}}".
 *
 * @property integer $id
 * @property integer $sms_id
 * @property string $summary
 * @property integer $type
 * @property string $compare_type
 */
class SmsSummaryRules extends \yii\db\ActiveRecord
{
                    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sms_summary_rules}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                                                                                                            
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['sms_id', 'type'], 'integer'],
            [['summary', 'compare_type'], 'string', 'max' => 255],
                                                                                                                            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sms_id' => 'Sms ID',
            'summary' => 'Summary',
            'type' => 'Type',
            'compare_type' => 'Compare Type',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\SmsSummaryRulesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\SmsSummaryRulesQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
            return $this->id;
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getSms_id()
    {
            return $this->sms_id;
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getSummary()
    {
            return $this->summary;
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getType()
    {
            return $this->type;
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getCompare_type()
    {
            return $this->compare_type;
    }
        
    /**
    * Relations
    **/

    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'sms_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'summary' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],    
        
    'type' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'compare_type' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],    
];}}
