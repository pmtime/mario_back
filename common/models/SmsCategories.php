<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%sms_categories}}".
 *
 * @property integer $id
 * @property integer $sms_id
 * @property integer $category_id
 */
class SmsCategories extends \yii\db\ActiveRecord
{
            
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sms_categories}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                                    
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['sms_id', 'category_id'], 'integer'],
                                                                                    ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sms_id' => 'Sms ID',
            'category_id' => 'Category ID',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\SmsCategoriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\SmsCategoriesQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
            return $this->id;
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getSms_id()
    {
            return $this->sms_id;

    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getCategory_id()
    {
            return $this->category_id;
    }
        
    /**
    * Relations
    **/

    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'sms_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'category_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
];}}
