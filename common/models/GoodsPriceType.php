<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%goods_price_type}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $price
 *
 * @property Goods[] $goods
 */
class GoodsPriceType extends \yii\db\ActiveRecord
{
                
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%goods_price_type}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                                                
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['type', 'price'], 'integer'],
                                                                                    ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Размер',
            'price' => 'Стоимость',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\GoodsPriceTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\GoodsPriceTypeQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){ 
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getType()
    {
        if(!empty($this->type)){ 
            return $this->type;
        }else{
            throw new UnknownPropertyException($this->type.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getPrice()
    {
        if(!empty($this->price)){ 
            return $this->price;
        }else{
            throw new UnknownPropertyException($this->price.', пуст в записи'. $this->id);
        }
    }
        
    /**
    * Relations
    **/

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedGoods()
    {
    return $this->hasMany(Goods::className(), ['price_id' => 'id']);
    }

    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'type' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'price' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
];}}
