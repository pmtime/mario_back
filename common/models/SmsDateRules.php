<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%sms_dates_rules}}".
 *
 * @property integer $id
 * @property string $sms_id
 * @property string $date
 * @property integer $type
 * @property string $date_to
 */
class SmsDateRules extends \yii\db\ActiveRecord
{
                    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sms_dates_rules}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                                                                                                            
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['sms_id'], 'required'],
            [['type'], 'integer'],
            [['sms_id', 'date', 'date_to'], 'string', 'max' => 255],
                                                                                                                            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sms_id' => 'Sms ID',
            'date' => 'Date',
            'type' => 'Type',
            'date_to' => 'Date To',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\SmsDateRulesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\SmsDateRulesQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
            return $this->id;
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getSms_id()
    {
            return $this->sms_id;
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getDate()
    {
            return $this->date;
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getType()
    {
            return $this->type;
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getDate_to()
    {
            return $this->date_to;
    }
        
    /**
    * Relations
    **/

    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'sms_id' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],    
        
    'date' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],    
        
    'type' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'date_to' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],    
];}}
