<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%ordered_goods}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $good_id
 * @property integer $count
 *
 * @property Goods $goods
 * @property Goods $good
 * @property Orders $order
 * @property Orders $orders
 */
class OrderedGoods extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ordered_goods}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                                                                                                                        
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['order_id', 'good_id', 'price_id','count'], 'required'],
            [['order_id', 'good_id', 'count','thickness'], 'integer'],
            [['good_id'], 'exist', 'skipOnError' => true, 'targetClass' => Goods::className(), 'targetAttribute' => ['good_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
                                                                                                        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'good_id' => 'Товар',
            'count' => 'Кол-во',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\OrderedGoodsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\OrderedGoodsQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){ 
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getOrder_id()
    {
        if(!empty($this->order_id)){ 
            return $this->order_id;
        }else{
            throw new UnknownPropertyException($this->order_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getGood_id()
    {
        if(!empty($this->good_id)){ 
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->good_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getCount()
    {
        if(!empty($this->count)){ 
            return $this->count;
        }else{
            throw new UnknownPropertyException($this->count.', пуст в записи'. $this->id);
        }
    }
        
    /**
    * Relations
    **/

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedGoods()
    {
    return $this->hasOne(Goods::className(), ['id' => 'good_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedGood()
    {
    return $this->hasOne(Goods::className(), ['id' => 'good_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedOrder()
    {
    return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedOrders()
    {
    return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

	public function getRelatedDia(){
		$model = GoodsPriceType::find()->andFilterWhere(['good_id'=>$this->good_id,'id'=>$this->price_id])->one();
		if(!is_null($model)){
			return $model->dia;
		}else{
			return 0;
		}
	}

	public function getRelatedPrice(){
		$this->hasOne(GoodsPriceType::className(),['id'=>'price_id'])->one();
	}

	public function getPrice(){
		$priceModel = $this->hasOne(GoodsPriceType::className(),['id'=>'price_id'])->one();
		if($priceModel->dia != 0){
			return $priceModel->dia;
		}else{
			return $priceModel->price;
		}
	}

    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
	    'order_id' => [
	        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
	        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
	        'options' => [
	        'prompt' => 'Выберите категорию',
	        ],
	    ],


	    'good_id' => [
	        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
	        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
	        'options' => [
	        'prompt' => 'Выберите категорию',
	        ],
	    ],
    
        
	    'count' => [
	        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
	        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
	        'options' => [
	        'prompt' => 'Выберите категорию',
	        ],
	    ],
    
];}}
