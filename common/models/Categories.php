<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%categories}}".
 *
 * @property integer $id
 * @property string $label
 *
 * @property Compliments $id0
 * @property Goods $id1
 * @property Goods[] $goods
 */
class Categories extends \yii\db\ActiveRecord
{
                    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%categories}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                                    
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['label'], 'string', 'max' => 255],
                                                                ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Название',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\CategoriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\CategoriesQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){ 
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getLabel()
    {
        if(!empty($this->label)){ 
            return $this->label;
        }else{
            throw new UnknownPropertyException($this->label.', пуст в записи'. $this->id);
        }
    }
        
    /**
    * Relations
    **/

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedId0()
    {
    return $this->hasOne(Compliments::className(), ['category_id' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedId1()
    {
    return $this->hasOne(Goods::className(), ['category_id' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedGoods()
    {
    return $this->hasMany(Goods::className(), ['category_id' => 'id']);
    }

    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'label' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],    
];}
	public function extraFields()
	{
		return ['relatedGoods','relatedGoods.relatedPrice'];
	}
}
