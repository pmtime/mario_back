<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use \yii\helpers\Html;
use \yii\base\UnknownPropertyException;
use yii\db\Expression;
use trntv\filekit\widget\Upload;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%vacancies_answers}}".
 *
 * @property integer $id
 * @property integer $vacancy_id
 * @property integer $status_id
 * @property string $name
 * @property string $second_name
 * @property string $phone
 * @property string $vacancy
 * @property string $text
 *
 * @property Vacancies $vacancy0
 */
class VacanciesAnswers extends \yii\db\ActiveRecord
{
                                    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vacancies_answers}}';
    }
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
    return [                                                                                                                                                                                                                                                                                                                    
    ];

    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['vacancy_id', 'status_id'], 'required'],
            [['vacancy_id', 'status_id'], 'integer'],
            [['name', 'second_name', 'phone', 'vacancy', 'text'], 'string', 'max' => 255],
                                                                                                                                                                                        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vacancy_id' => 'Vacancy ID',
            'status_id' => 'Status ID',
            'name' => 'Name',
            'second_name' => 'Second Name',
            'phone' => 'Phone',
            'vacancy' => 'Vacancy',
            'text' => 'Text',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\VacanciesAnswersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\VacanciesAnswersQuery(get_called_class());
    }

    /**
    * Access methods for the table attributes
    **/

        
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getId()
    {
        if(!empty($this->id)){ 
            return $this->id;
        }else{
            throw new UnknownPropertyException($this->id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getVacancy_id()
    {
        if(!empty($this->vacancy_id)){ 
            return $this->vacancy_id;
        }else{
            throw new UnknownPropertyException($this->vacancy_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getStatus_id()
    {
        if(!empty($this->status_id)){ 
            return $this->status_id;
        }else{
            throw new UnknownPropertyException($this->status_id.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getName()
    {
        if(!empty($this->name)){ 
            return $this->name;
        }else{
            throw new UnknownPropertyException($this->name.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getSecond_name()
    {
        if(!empty($this->second_name)){ 
            return $this->second_name;
        }else{
            throw new UnknownPropertyException($this->second_name.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getPhone()
    {
        if(!empty($this->phone)){ 
            return $this->phone;
        }else{
            throw new UnknownPropertyException($this->phone.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getVacancy()
    {
        if(!empty($this->vacancy)){ 
            return $this->vacancy;
        }else{
            throw new UnknownPropertyException($this->vacancy.', пуст в записи'. $this->id);
        }
    }
            
    /**
    * @return string
    * @throws \yii\base\UnknownPropertyException;
    */
    public function getText()
    {
        if(!empty($this->text)){ 
            return $this->text;
        }else{
            throw new UnknownPropertyException($this->text.', пуст в записи'. $this->id);
        }
    }
        
    /**
    * Relations
    **/

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRelatedVacancy0()
    {
    return $this->hasOne(Vacancies::className(), ['id' => 'vacancy_id']);
    }

    /**
    * Admin form generator
    **/
public function getFormConfig(){
return [
    
        
    'vacancy_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'status_id' => [
        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
        'items' => ArrayHelper::map(Categories::find()->all(),'id','label'),
        'options' => [
        'prompt' => 'Выберите категорию',
        ],
    ],
    
        
    'name' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],    
        
    'second_name' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],    
        
    'phone' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],    
        
    'vacancy' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],    
        
    'text' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
    ],    
];}}
