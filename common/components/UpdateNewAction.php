<?php
	/**
	 * Created by PhpStorm.
	 * User: filipp
	 * Date: 18.08.16
	 * Time: 2:00 PM
	 */
	
	namespace common\components;
	
	use yii\db\ActiveRecord;
	use yii\rest\UpdateAction;

	class UpdateNewAction extends UpdateAction
	{
		public function run($id)
		{
			/* @var $model ActiveRecord */
			$model = $this->findModel($id);

			if ($this->checkAccess) {
				call_user_func($this->checkAccess, $this->id, $model);
			}

			$model->scenario = $this->scenario;
			$model->load(\Yii::$app->getRequest()->getBodyParams(), '');
			$model->detachBehaviors('relatedGoo');
			if ($model->save() === false && !$model->hasErrors()) {
				throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
			}

			return $model;



		}
	}