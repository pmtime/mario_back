<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PaymentHistory */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => $this->context->labelMany, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-history-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
