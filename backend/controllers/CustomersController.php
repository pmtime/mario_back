<?php

namespace backend\controllers;

use common\models\SmsSolo;
use Yii;
use common\models\Customers;
use backend\models\search\CustomersSearch;
use backend\components\BackendController;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Sharoff\Mango\Api\MangoHelper;
/**
 * CustomersController implements the CRUD actions for Customers model.
 */
class CustomersController extends BackendController
{
    private $command_id;
	private $call_id;
    public $labelMany = 'Клиенты';
    public $labelOne = 'Клиент';
    

    
    public function getModelClass(){
        return  Customers::className();
    }
    
    public function getModelSearchClass(){
        return  CustomersSearch::className();
    }

	public function actionSms ( )
	{
		$model = new SmsSolo();
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->session->setFlash('customer_sms','Сообщение отправлено');
			if($this->sendSms ( $model->text , $model->getCustomer()->one()->getPhone () )){
				$model->status = 1;
				$model->update();
			}

			return $this->redirect(['view','id'=>$model->customer_id]);
		} else {
			\Yii::$app->session->setFlash('customer_sms','Сообщение не отправлено');
			return $this->redirect(['index']);
		}

	}

	public function sendSms ( $text , $phone )
	{
		\Yii::$app->sms->sms_send ( $phone , $text);
		return true;
	}
	
	
}
