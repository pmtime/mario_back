<?php

namespace backend\controllers;

use Yii;
use common\models\Vacancies;
use backend\models\search\VacanciesSearch;
use backend\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VacanciesController implements the CRUD actions for Vacancies model.
 */
class VacanciesController extends BackendController
{
    
    public $labelMany = 'Вакансии';
    public $labelOne = 'Вакансия';
    

    
    public function getModelClass(){
    return  Vacancies::className();
    }
    
    public function getModelSearchClass(){
    return  VacanciesSearch::className();
    }
}
