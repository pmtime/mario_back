<?php

	namespace backend\controllers;

	use common\models\GoodsPriceType;
	use Yii;
	use common\models\Goods;
	use backend\models\search\GoodsSearch;
	use backend\components\BackendController;
	use yii\helpers\VarDumper;
	use yii\web\NotFoundHttpException;
	use yii\filters\VerbFilter;
	use himiklab\sortablegrid\SortableGridAction;

	/**
	 * GoodsController implements the CRUD actions for Goods model.
	 */
	class GoodsController extends BackendController
	{

		public $labelMany = 'Торвары';

		public $labelOne = 'Товар';

		public function actions ()
		{

			return [
				'sort' => [
					'class'     => SortableGridAction::className () ,
					'modelName' => $this->getModelClass () ,
				] ,
			];
		}

		public function getModelClass ()
		{

			return Goods::className ();
		}

		public function getModelSearchClass ()
		{

			return GoodsSearch::className ();
		}

		public function actionIndex ()
		{

			// Check if there is an Editable ajax request
			if ( isset( $_POST[ 'hasEditable' ] ) ) {
				$model = $this->findModel ( $_POST[ 'editableKey' ] );
				$model->setAttributes ( $_POST[ $this->getModel () ] , FALSE );

				// use Yii's response format to encode output as JSON
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				// read your posted model attributes
				$m = $this->getModelClass ();
				$model->setAttributes ( $_POST[ $this->getModel () ][ 0 ] , FALSE );
				if ( Yii::$app->db->createCommand ()->update ( $m::tableName () , $_POST[ $this->getModel () ][ 0 ] , 'id = ' . $_POST[ 'editableKey' ] )->execute () ) {
					return [ 'output'  => $model->priority ,
					         'message' => '',
					];
				}

				else {
					return [ 'output'  => '' ,
					         'message' => '',
					];
				}
			}

			$searchModel = new $this->modelSearchClass();
			$dataProvider = $searchModel->search ( Yii::$app->request->queryParams );
			$dataProvider->sort = [
				'defaultOrder' => [ 'sortOrder' => SORT_DESC ],
			];

			return $this->render ( 'index' , [
				'searchModel'  => $searchModel ,
				'dataProvider' => $dataProvider ,
			] );
		}

		public function actionCreatePizza ()
		{

			$model = new $this->modelClass();

			if ( $model->load ( Yii::$app->request->post () ) && $model->save () ) {
				return $this->redirect ( [ 'index' ] );
			}
			else {
				return $this->render ( 'create_pizza' , [
					'model' => $model ,
				] );
			}
		}

		public function actionUpdatePizza ( $id )
		{

			$model = $this->findModel ( $id );
			//echo ddd(Yii::$app->request->post());
			//exit;
			if ( $model->load ( Yii::$app->request->post () ) && $model->save () ) {
				return $this->redirect ( [ 'index' ] );
			}
			else {
				return $this->render ( 'update_pizza' , [
					'model' => $model ,
				] );
			}
		}

		public function actionUpdate ( $id )
		{

			$model = $this->findModel ( $id );
			/*$prices = Yii::$app->request->post(\yii\helpers\StringHelper::basename(get_class($model)));
			if($prices['prices']){
				foreach ($prices['prices'] as $newPrice){
					$price = new GoodsPriceType();
					$price->setAttributes($newPrice);
					$price->save();
				}
			}*/
			if ( $model->load ( Yii::$app->request->post () ) && $model->save () ) {
				return $this->redirect ( [ 'index' ] );
			}
			else {
				return $this->render ( 'update' , [
					'model' => $model ,
				] );
			}
		}
	}
