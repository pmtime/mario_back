<?php

namespace backend\controllers;

use Yii;
use common\models\Deliveries;
use backend\models\search\DeliveriesSearch;
use backend\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DeliveriesController implements the CRUD actions for Deliveries model.
 */
class DeliveriesController extends BackendController
{
    
    public $labelMany = 'Варианты доставки';
    public $labelOne = 'Вариант доставки';
    

    
    public function getModelClass(){
    return  Deliveries::className();
    }
    
    public function getModelSearchClass(){
    return  DeliveriesSearch::className();
    }
}
