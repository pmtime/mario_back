<?php

	namespace backend\controllers;

	use common\models\Customers;
	use common\models\Orders;
	use common\models\SmsRecepients;
	use Yii;
	use common\models\Sms;
	use backend\models\search\SmsSearch;
	use backend\components\BackendController;
	use yii\helpers\Html;
	use yii\web\NotFoundHttpException;
	use yii\filters\VerbFilter;
	use yii\helpers\Url;

	/**
	 * SmsController implements the CRUD actions for Sms model.
	 */
	class SmsController extends BackendController
	{

		public $labelMany = 'SMS Рассылки';

		public $labelOne = 'SMS Рассылка';

		public function getModelClass ()
		{

			return Sms::className ();
		}

		public function getModelSearchClass ()
		{

			return SmsSearch::className ();
		}

		public function actionSend ( $id )
		{

			/*
			 * Find current \common\models\Sms;
			 * */
			$model = $this->findModel ( $id );
			/*
			 * intialize Orders finder
			 * */
			




				$findClients = SmsRecepients::find()->andFilterWhere(['like','sms_id',$model->id])->all();

				if ( !empty( $findClients ) ) {

					foreach ( $findClients as $client ) {
						if($this->sendSms ( $client->getCustomer()->one()->getFullName () , $client->getCustomer()->one()->getPhone (), $model->getTemplate() )){
							$client->status = 1;
							$client->update();
						}

						$this->redirect(Url::to(['sms/view', 'id' => $model->id]));

					}

				}




		}
		/*
		 * Send sms function
		 *  */
		public function sendSms ( $name , $phone, $model )
		{
			$string = $model->one()->getTemplate();
			$client_message = str_replace('{$name}',$name,$string);
			\Yii::$app->sms->sms_send ( $phone , $client_message);
			return true;
		}

	}
