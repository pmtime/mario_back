<?php

namespace backend\controllers;

use Yii;
use common\models\Payment;
use backend\models\search\PaymentSearch;
use backend\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaymentController implements the CRUD actions for Payment model.
 */
class PaymentController extends BackendController
{
    
    public $labelMany = 'Payment';
    public $labelOne = 'Payment';
    

    
    public function getModelClass(){
    return  Payment::className();
    }
    
    public function getModelSearchClass(){
    return  PaymentSearch::className();
    }
}
