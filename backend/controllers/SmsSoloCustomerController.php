<?php

namespace backend\controllers;

use backend\models\search\SmsSearch;
use Yii;
use common\models\SmsSoloCustomer;
use backend\models\search\SmsSoloCustomer as SmsSoloCustomerSearch;
use backend\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SmsSoloCustomerController implements the CRUD actions for SmsSoloCustomer model.
 */
class SmsSoloCustomerController extends BackendController
{
    
    public $labelMany = 'SmsSoloCustomer';
    public $labelOne = 'SmsSoloCustomer';
    

    
    public function getModelClass(){
        return  SmsSoloCustomer::className();
    }
    
    public function getModelSearchClass(){
        return  SmsSearch::className();
    }
}
