<?php

	namespace backend\controllers;

	use Yii;
	use common\models\Promocodes;
	use backend\models\search\PromocodesSearch;
	use backend\components\BackendController;
	use yii\web\NotFoundHttpException;
	use yii\filters\VerbFilter;

	/**
	 * PromocodesController implements the CRUD actions for Promocodes model.
	 */
	class PromocodesController extends BackendController
	{

		public $labelMany = 'Промокоды';

		public $labelOne = 'Промокод';

		public function getModelClass ()
		{

			return Promocodes::className ();
		}

		public function getModelSearchClass ()
		{

			return PromocodesSearch::className ();
		}

		public function actionCreate ()
		{

			$model = new $this->modelClass();

			if ( $model->load ( Yii::$app->request->post () ) ) {
				if ( $model->count > 0 ) {

					for ( $i = 1; ; $i++ ) {
						if($i > $model->count){
							break;
						}
						$model = new Promocodes();
						$model->setAttributes($model->load( Yii::$app->request->post ()));
						$model->serial = substr ( md5 ( microtime () ) , rand ( 0 , 26 ) , 6 );
						$model->save ();
					}

					return $this->redirect ( [ 'index' ] );
				}
				else {
					$model->save ();

					return $this->redirect ( [ 'index' ] );

				}
			}
			else {
				return $this->render ( 'create' , [
					'model' => $model ,
				] );
			}
		}

	}
