<?php

namespace backend\controllers;

use Yii;
use common\models\Compliments;
use backend\models\search\ComplimentsSearch;
use backend\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ComplimentsController implements the CRUD actions for Compliments model.
 */
class ComplimentsController extends BackendController
{
    
    public $labelMany = 'Compliments';
    public $labelOne = 'Compliments';
    

    
    public function getModelClass(){
    return  Compliments::className();
    }
    
    public function getModelSearchClass(){
    return  ComplimentsSearch::className();
    }
}
