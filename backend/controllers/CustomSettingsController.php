<?php

namespace backend\controllers;

use Yii;
use common\models\CustomSettings;
use backend\models\search\CustomSettingsSearch;
use backend\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CustomSettingsController implements the CRUD actions for CustomSettings model.
 */
class CustomSettingsController extends BackendController
{
    
    public $labelMany = 'Настройки';
    public $labelOne = 'Настройки';
    

    
    public function getModelClass(){
    return  CustomSettings::className();
    }
    
    public function getModelSearchClass(){
    return  CustomSettingsSearch::className();
    }
}
