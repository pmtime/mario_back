<?php

namespace backend\controllers;

use Yii;
use common\models\Callback;
use backend\models\search\CallbackSearch;
use backend\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Sharoff\Mango\Api\MangoHelper;
/**
 * CallbackController implements the CRUD actions for Callback model.
 */
class CallbackController extends BackendController
{
    
    public $labelMany = 'Обратная связь';
    public $labelOne = 'Обратная связь';
    

    
    public function getModelClass(){
        return  Callback::className();
    }
    
    public function getModelSearchClass(){
        return  CallbackSearch::className();
    }

	public function actionCall ( $id )
	{
		$find = Callback::findOne($id);
		$set = MangoHelper::setApiKey('dvp7f2cym36xbh2qhfxofkmb9cq6u0hx')
			->setApiSalt('lwvrpxf1qcpsjh9rnw5t7iu6ka351btc');
		$data = MangoHelper::sendCall('113', $find->getRelatedCustomer()->one()->getPhone());
		return $this->redirect(['view','id'=>$id]);
	}
}
