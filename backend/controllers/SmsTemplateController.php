<?php

namespace backend\controllers;

use Yii;
use common\models\SmsTemplate;
use backend\models\search\SmsTemplateSearch;
use backend\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SmsTemplateController implements the CRUD actions for SmsTemplate model.
 */
class SmsTemplateController extends BackendController
{
    
    public $labelMany = 'SmsTemplate';
    public $labelOne = 'SmsTemplate';
    

    
    public function getModelClass(){
    return  SmsTemplate::className();
    }
    
    public function getModelSearchClass(){
    return  SmsTemplateSearch::className();
    }
}
