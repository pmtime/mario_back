<?php

namespace backend\controllers;

use Yii;
use common\models\Categories;
use backend\models\search\CategoriesSearch;
use backend\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoriesController implements the CRUD actions for Categories model.
 */
class CategoriesController extends BackendController
{
    
    public $labelMany = 'Категории';
    public $labelOne = 'Категория';
    

    
    public function getModelClass(){
    return  Categories::className();
    }
    
    public function getModelSearchClass(){
    return  CategoriesSearch::className();
    }
}
