<?php

	namespace backend\controllers;

	use common\models\Goods;
	use common\models\GoodsPriceType;
	use kartik\mpdf\Pdf;
	use Yii;
	use common\models\Orders;
	use backend\models\search\OrdersSearch;
	use backend\components\BackendController;
	use yii\web\NotFoundHttpException;
	use yii\filters\VerbFilter;
	use Sharoff\Mango\Api\MangoHelper;
	use yii\helpers\Json;

	/**
	 * OrdersController implements the CRUD actions for Orders model.
	 */
	class OrdersController extends BackendController
	{

		public $labelMany = 'Заказы';

		public $labelOne = 'Заказ';

		public function getModelClass ()
		{

			return Orders::className ();
		}

		public function getModelSearchClass ()
		{

			return OrdersSearch::className ();
		}

		public function actionCall ( $id )
		{
			$find = Orders::findOne ( $id );
			$set = MangoHelper::setApiKey ( 'dvp7f2cym36xbh2qhfxofkmb9cq6u0hx' )->setApiSalt ( 'lwvrpxf1qcpsjh9rnw5t7iu6ka351btc' );
			$call = MangoHelper::sendCall ( '113' , $find->getRelatedCustomer ()->one()->getPhone() );
			return $this->redirect ( [
				'view' ,
				'id' => $id,
			] );
		}

		public function actionGoodDia ()
		{
			$id = $_GET['id'];
			$dia = GoodsPriceType::find()->andWhere(['good_id'=>$id])->all();
			foreach($dia as $price){
				if($price->dia != 0){
					echo "<option value='".$price->id."'>".$price->dia." см</option>";
				}else{
					echo "<option value='".$price->id."'>".$price->price." руб.</option>";

				}
			}
		}

		public function actionGoodThikness ()
		{

			$out = [ ];
			$id = array_pop ( $_POST[ 'depdrop_parents' ] );
			$find = GoodsPriceType::find ()->andWhere ( [ 'id' => $id ] )->one ();
			if ( !empty( $find->dia ) ) {
				$out[] = [
					'id'   => 1 ,
					'name' => 'Тонкая',
				];
				$out[] = [
					'id'   => 2 ,
					'name' => 'Обычная',
				];
				$selected = '1';
				// Shows how you can preselect a value
				echo Json::encode ( [
					'output'   => $out ,
					'selected' => $selected,
				] );

				return;
			}
			else {
				echo Json::encode ( [
					'output'   => '' ,
					'selected' => '',
				] );
			}

		}

		public function actionPdfOrders ()
		{

			$model = $this->getModelClass ();
			$orders = $model::find ()->all ();
			$content = '';
			foreach ( $orders as $order ) {
				$content .= $this->renderPartial ( 'viewReport' , [ 'model' => $order ] );

			}
			// setup kartik\mpdf\Pdf component
			$pdf = new Pdf( [
				// set to use core fonts only
				'mode'        => Pdf::MODE_UTF8 ,
				// A4 paper format
				'format'      => Pdf::FORMAT_A4 ,
				// portrait orientation
				'orientation' => Pdf::ORIENT_PORTRAIT ,
				// stream to browser inline
				'destination' => Pdf::DEST_DOWNLOAD ,
				// your html content input
				'content'     => $content ,
				// format content from your own css file if needed or use the
				// enhanced bootstrap css built by Krajee for mPDF formatting
				'cssFile'     => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css' ,
				// any css to be embedded if required
				'cssInline'   => '.kv-heading-1{font-size:18px}' ,
				// set mPDF properties on the fly
				'options'     => [ 'title' => 'Заказы' ] ,
				'filename'    => 'Заказы | Дата выгрузки ' . date ( 'd.m.y' ) . '.pdf' ,
				// call mPDF methods on the fly
				'methods'     => [
					'SetHeader' => [ 'Заказы' ] ,
					'SetFooter' => [ '{PAGENO}' ] ,
				],
			] );

			// return the pdf output as per the destination setting
			return $pdf->render ();
		}

		public function actionPrintOrder ( $id )
		{

			$model = $this->findModel ( $id );
			$content = $this->renderPartial ( 'viewReport' , [ 'model' => $model ] );
			// setup kartik\mpdf\Pdf component
			$pdf = new Pdf( [
				// set to use core fonts only
				'mode'        => Pdf::MODE_UTF8 ,
				// A4 paper format
				'format'      => Pdf::FORMAT_A4 ,
				// portrait orientation
				'orientation' => Pdf::ORIENT_PORTRAIT ,
				// stream to browser inline
				'destination' => Pdf::DEST_BROWSER ,
				// your html content input
				'content'     => $content ,
				// format content from your own css file if needed or use the
				// enhanced bootstrap css built by Krajee for mPDF formatting
				'cssFile'     => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css' ,
				// any css to be embedded if required
				'cssInline'   => '.kv-heading-1{font-size:18px}' ,
				// set mPDF properties on the fly
				'options'     => [ 'title' => 'Заказы' ] ,
				// call mPDF methods on the fly
				'methods'     => [
					'SetHeader' => [ 'Заказ №: ' . $model->id . '.pdf' ] ,
					'SetFooter' => [ '{PAGENO}' ] ,
				],
			] );

			// return the pdf output as per the destination setting
			return $pdf->render ();
		}

		public function actionRecalculate ()
		{

			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			
			
			$resp = [ ];
			$promo = ( isset($_POST[ 'Orders' ][ 'promocode_id' ]) ) ? $_POST[ 'Orders' ][ 'promocode_id' ] : false;
			foreach ( $_POST[ 'Orders' ][ 'relatedGoods' ] as $good ) {
				$resp[] += Goods::getGoodCalculatedPrice ( 
					$good[ 'good_id' ] , 
					$good[ 'price_id' ] , 
					$good[ 'count' ] ,
					$_POST[ 'Orders' ][ 'relatedCompliments' ] ,
					$promo
				);
			}

			return [
				'response' => array_sum ( $resp ),
			];
		}
	}
