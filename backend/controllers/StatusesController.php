<?php

namespace backend\controllers;

use Yii;
use common\models\Statuses;
use backend\models\search\StatusesSearch;
use backend\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StatusesController implements the CRUD actions for Statuses model.
 */
class StatusesController extends BackendController
{
    
    public $labelMany = 'Статусы';
    public $labelOne = 'Статус';
    

    
    public function getModelClass(){
    return  Statuses::className();
    }
    
    public function getModelSearchClass(){
    return  StatusesSearch::className();
    }
}
