<?php

namespace backend\controllers;

use Yii;
use common\models\PaymentHistory;
use backend\models\search\PaymentHistorySearch;
use backend\components\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaymentHistoryController implements the CRUD actions for PaymentHistory model.
 */
class PaymentHistoryController extends BackendController
{
    
    public $labelMany = 'PaymentHistory';
    public $labelOne = 'PaymentHistory';
    

    
    public function getModelClass(){
    return  PaymentHistory::className();
    }
    
    public function getModelSearchClass(){
    return  PaymentHistorySearch::className();
    }
}
