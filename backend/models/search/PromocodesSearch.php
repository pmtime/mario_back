<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Promocodes;

/**
 * PromocodesSearch represents the model behind the search form about `common\models\Promocodes`.
 */
class PromocodesSearch extends Promocodes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sale', 'multiple', 'status_id', 'author_id', 'updater_id', 'update_time'], 'integer'],
            [['serial', 'from', 'to', 'create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Promocodes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'from' => $this->from,
            'to' => $this->to,
            'sale' => $this->sale,
            'multiple' => $this->multiple,
            'status_id' => $this->status_id,
            'author_id' => $this->author_id,
            'updater_id' => $this->updater_id,
            'update_time' => $this->update_time,
        ]);

        $query->andFilterWhere(['like', 'serial', $this->serial])
            ->andFilterWhere(['like', 'create_time', $this->create_time]);

        return $dataProvider;
    }
}
