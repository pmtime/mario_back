<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $this->context->labelMany;
$this->params['breadcrumbs'][] = $this->title;
    if(Yii::$app->user->can('administrator')){
        $actions = '{view}{update}{delete}';
    }else{
        $actions = '{view}{update}';
    }
?>
<div class="customers-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create ').$this->context->labelOne, ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<p><?php echo $messages = \Yii::$app->session->getFlash('customer_sms'); ?></p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'phone',
            'fullName',
	            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $actions ,
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
.(Yii::$app->user->can('administrator')) ? ' {delete}' : '' ,