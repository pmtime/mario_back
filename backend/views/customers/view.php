<?php

	use common\models\SmsRecepients;
	use common\models\SmsSolo;
	use yii\helpers\Html;
	use yii\widgets\DetailView;
	use yii\widgets\ActiveForm;

	/* @var $this yii\web\View */
	/* @var $model common\models\Customers */

	$this->title = $this->context->labelOne;
	$this->params[ 'breadcrumbs' ][] = [
		'label' => $this->context->labelMany ,
		'url'   => [ 'index' ],
	];
	$this->params[ 'breadcrumbs' ][] = $this->title;
?>
<div class="customers-view">

	<p>
		<?= Html::a ( Yii::t ( 'app' , 'Редактировать' ) , [
			'update' ,
			'id' => $model->id,
		] , [ 'class' => 'btn btn-primary' ] ) ?>
		<?= Html::a ( Yii::t ( 'app' , 'Удалить' ) , [
			'delete' ,
			'id' => $model->id,
		] , [
			'class' => 'btn btn-danger' ,
			'data'  => [
				'confirm' => Yii::t ( 'app' , 'Are you sure you want to delete this item?' ) ,
				'method'  => 'post' ,
			] ,
		] ) ?>
	</p>
	<p>
	<h4>Написать SMS</h4>
	<p><?php echo $messages = \Yii::$app->session->getFlash('customer_sms'); ?></p>
	<?php
	$sms = new SmsSolo();
	$form = ActiveForm::begin ( [
			'action' =>['customers/sms'],
			'id' => 'send-sms' ,
		] ); ?>
	<?php echo $form->field ( $sms , 'text' )->textarea () ?>
	<?php echo $form->field ( $sms , 'customer_id' )->hiddenInput ( [ 'value' => $model->id ] )->label ( FALSE ) ?>
	<?php echo $form->field ( $sms , 'status' )->hiddenInput ( [ 'value' => 0 ] )->label ( FALSE ) ?>
	<?= Html::submitButton ( Yii::t ( 'app' , 'Отправить' ) , [ 'class' => 'btn btn-primary' ] ) ?>
	<?php ActiveForm::end (); ?>

	</p>
	<h4>Отправленные SMS</h4>
	<?php foreach($model->getSmsSolo()->all() as $smsSolo) {
		echo $smsSolo->create_time .' - '.$smsSolo->getText(). ' - ' . $smsSolo->getStatus() . '<br>';
	} ?>
	<?= DetailView::widget ( [
		'model' => $model ,
		'attributes' => [
			'id' ,
			'phone' ,
			'fullName' ,
			'create_time' ,
			'update_time' ,
		] ,
	] ) ?>

</div>
