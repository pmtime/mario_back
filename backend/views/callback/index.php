<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CallbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $this->context->labelMany;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="callback-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'relatedCustomer.fullName',
            'relatedCustomer.phone',
            'create_time',
            [
	            'class' => 'yii\grid\ActionColumn',
	            'header'=>'Действия',
	            'headerOptions' => ['width' => '150'],
	            'template' => '{view}{delete}{link}',
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
