<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Vacancies */

$this->title = $this->context->labelOne;
$this->params['breadcrumbs'][] = ['label' => $this->context->labelMany, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacancies-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
	<h2>Ответы на вакансию</h2>
	<?php
		foreach ( $model->getRelatedVacanciesAnswers()->all() as $item ) {
			echo DetailView::widget([
				'model' => $item,
				'attributes' => [
					'name',
					'second_name',
					'phone',
					'text',
				],
			]);
		} ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'functions:ntext',
            'offer:ntext',
            'compensation_from',
            'compensation_to',
            'author_id',
            'updater_id',
            'create_time',
            'update_time',
            'status_id',
        ],
    ]) ?>

</div>
