<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\sortable\Sortable;
/* @var $this yii\web\View */
/* @var $model common\models\Categories */

$this->title = $this->context->labelOne;
$this->params['breadcrumbs'][] = ['label' => $this->context->labelMany, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'label',
        ],
    ]) ?>
	<?php
		echo Sortable::widget([
			'type'=>'grid',
			'pluginEvents' =>[
				'sortupdate' => 'function(e) { 
					console.log(e); 
					console.log(e.detail.index); 
					console.log(e.detail); 
					console.log(e.detail.oldindex); 
					console.log(e.detail.elementIndex); 
					console.log(e.detail.oldElementIndex); 
					console.log(e.detail.startparent); 
					console.log(e.detail.endparent); 
				}'
			],
			'items'=>[
				['content'=>'<div id="1" class="grid-item text-danger">Item 1</div>'],
				['content'=>'<div id="2" class="grid-item text-danger">Item 2</div>'],
				['content'=>'<div id="3" class="grid-item text-danger">Item 3</div>'],
				['content'=>'<div id="4" class="grid-item text-danger">Item 4</div>'],
				['content'=>'<div id="5" class="grid-item text-danger">Item 5</div>'],
				['content'=>'<div id="6" class="grid-item text-danger">Item 6</div>'],
				['content'=>'<div id="7" class="grid-item text-danger">Item 7</div>'],
				['content'=>'<div id="8" class="grid-item text-danger">Item 8</div>'],
				['content'=>'<div id="9" class="grid-item text-danger">Item 9</div>'],
				['content'=>'<div id="10" class="grid-item text-danger">Item 10</div>'],
			]
		]); ?>
</div>
