<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Statuses */

$this->title = Yii::t('app', 'Создание');
$this->params['breadcrumbs'][] = ['label' => $this->context->labelMany, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="statuses-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
