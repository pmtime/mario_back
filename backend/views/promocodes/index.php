<?php

	use common\models\Promocodes;
	use yii\helpers\Html;
	use kartik\export\ExportMenu;
	use kartik\grid\GridView;
	use yii\widgets\Pjax;
	use yii\helpers\ArrayHelper;
	use common\models\Statuses;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PromocodesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $this->context->labelMany;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promocodes-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать ').$this->context->labelOne, ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?php Pjax::begin(); ?>
	<?php
		$gridcol = [
			['class' => 'yii\grid\SerialColumn'],
			'serial',
			'from',
			'to',
			'sale',
			[
				'attribute'=>'status_id',
				'value'=> function($model){
					return ($model->status_id == 6) ? 'Использован' : 'Не использован';
				},
				'filter'          => Html::activeDropDownList ( $searchModel , 'status_id' , ArrayHelper::map ( Statuses::find ()->andFilterWhere ( [ 'class' => Promocodes::className () ] )->asArray ()->all () , 'id' , 'label' ) , [
					'class'  => 'form-control' ,
					'prompt' => 'Выберите статус',
				] ) ,
			],
			[
				'attribute'=>'multiple',
				'header'=>'Кол-во активаций',
				'value'=>function($model){
					return $model->getCountOfActivatedPromo();
				}
			],
			['class' => 'yii\grid\ActionColumn'],
		]; ?>
	<?php echo ExportMenu::widget ( [
			'dataProvider'    => $dataProvider ,
			'columns'         => $gridcol ,
			'fontAwesome'     => TRUE ,
			'dropdownOptions' => [
				'label' => 'Export All' ,
				'class' => 'btn btn-default',
			],
		] ) . "<hr>\n" ?>
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridcol
    ]); ?>
<?php Pjax::end(); ?></div>
