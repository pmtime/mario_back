<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Sms */

$this->title = $this->context->labelOne;
$this->params['breadcrumbs'][] = ['label' => $this->context->labelMany, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>
<div class="sms-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
