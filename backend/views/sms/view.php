<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Sms */

$this->title = $this->context->labelOne;
$this->params['breadcrumbs'][] = ['label' => $this->context->labelMany, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-view">

    <p>
	    <?= Html::a(Yii::t('app', 'Разослать'), ['send', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
	    <?= Html::a(Yii::t('app', 'Редактировать рассылку'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить рассылку'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
	<?php
		$recepients = $model->getRecepients();
		echo "<h3>Всего найдено: {$recepients->count()} | Всего доставлено: {$recepients->andFilterWhere(['like','status',1])->count()}</h3>";

		echo "<h4>Получатели:<h4>";
		foreach($recepients->all() as $recepient){
			echo DetailView::widget([
				'model' => $recepient,
				'attributes' => [
					'customer.fullName',
					'customer.phone',
				],
			]);
		} ?>


</div>
