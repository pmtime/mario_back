<?php

	use common\models\Orders;
	use common\models\Statuses;
	use kartik\editable\Editable;
	use yii\helpers\ArrayHelper;
	use yii\helpers\Html;
	use kartik\export\ExportMenu;
	use kartik\grid\GridView;
	use yii\i18n\Formatter;
	use yii\widgets\Pjax;
	use Carbon\Carbon;
	/* @var $this yii\web\View */
	/* @var $searchModel backend\models\search\OrdersSearch */
	/* @var $dataProvider yii\data\ActiveDataProvider */

	$this->title = $this->context->labelMany;
	$this->params[ 'breadcrumbs' ][] = $this->title;
    if(Yii::$app->user->can('administrator')){
        $actions = '{view}{print-order}{call}{update}{delete}';
    }else{
        $actions = '{view}{print-order}{call}{update}';
    }
?>
<div class="orders-index">
	

	<p>
		<?= (Yii::$app->user->can('administrator')) ? Html::a ( Yii::t ( 'app' , 'Экспортировать ' ) . $this->context->labelMany , [ 'pdf-orders' ] , [ 'class' => 'btn btn-success' ] ) : '' ?>
		<?= Html::a ( Yii::t ( 'app' , 'Создать ' ) . $this->context->labelOne , [ 'create' ] , [ 'class' => 'btn btn-success' ] ) ?>
	</p>
	<?php
		$gridcol = [
			[ 'class' => 'yii\grid\SerialColumn' ] ,
			[
				'attribute' => 'create_time',
				'label'     => 'Дата' ,
				'width'     => '100px' ,
				'value' => function($model) {
					$date = new DateTime($model->create_time);
					$date->add(new DateInterval('PT3H'));
					return $date->format('d.m.Y H:i:s');
				}
			],

			[
				'attribute' => 'summary' ,
				'format'    => 'text' ,
				'label'     => 'Сумма' ,
				'width'     => '80px' ,
				'content'   => function ( $model , $key , $index , $column ) {

					return ( $model->summary != 0 ) ? $model->summary : 'Обратный звонок';
				} ,
			] ,
			/*[
				'attribute' => 'summary' ,
				'format'    => 'text' ,
				'label'     => 'Заказ' ,
				'content'   => function ( $model , $key , $index , $column ) {

					$goods = '';
					foreach ( $model->getRelatedGoods ()->all () as $good ) {
						$g = $good->getRelatedGood ()->one ();
						$dia = $good->getRelatedDia ();
						$goods .= ( $dia > 0 ) ? $g->label . ' - ' . $dia . ' кол-во ' . $good->count . '<br>' : $g->label . ' - кол-во ' . $good->count . '<br>';
					}
					$string = $goods;

					return $string;
				} ,
			] ,*/

			[
				'attribute' => 'relatedCustomer.fullName' ,
				'format'    => 'text' ,
				'label'     => 'Имя' ,
			] ,
			[
				'attribute' => 'relatedCustomer.phone' ,
				'format'    => 'text' ,
				'label'     => 'Телефон' ,
			] ,
			[
				'attribute' => 'relatedDelivery.label' ,
				'format'    => 'text' ,
				'label'     => 'Доставка' ,
			] ,
			[
				'attribute' => 'relatedPayment.label' ,
				'format'    => 'text' ,
				'label'     => 'Тип оплаты' ,
			] ,
			[
				'attribute' => 'relatedPaymentHistory.status' ,
				'format'    => 'text' ,
				'label'     => 'Статус оплаты' ,
				'content'   => function ( $model , $key , $index , $column ) {
						if( $model->relatedPaymentHistory ){
							if($model->relatedPaymentHistory->status == 1){
								return 'Оплачено';
							}else{
								return 'Неоплачено';
							}
						}else{
							 return 'Ожидает оплаты';
						}
				} ,
			] ,
			[
				'class'           => 'kartik\grid\EditableColumn' ,
				'attribute'       => 'status_id' ,
				'value'           => 'relatedStatus.label' ,
				'editableOptions' => [
					'header'    => 'Статус' ,
					'inputType' => Editable::INPUT_DROPDOWN_LIST ,
					'data'      => ArrayHelper::map ( Statuses::find ()->andFilterWhere ( [ 'class' => Orders::className () ] )->asArray ()->all () , 'id' , 'label' ) ,
					'options'   => [
						'pluginOptions' => [
							'min' => 0 ,
							'max' => 5000 ,
						] ,
					] ,
				] ,
				'hAlign'          => 'left' ,
				'vAlign'          => 'middle' ,
				'width'           => '200px' ,
				'format'          => 'text' ,
				'filter'          => Html::activeDropDownList ( $searchModel , 'status_id' , ArrayHelper::map ( Statuses::find ()->andFilterWhere ( [ 'class' => Orders::className () ] )->asArray ()->all () , 'id' , 'label' ) , [
					'class'  => 'form-control' ,
					'prompt' => 'Выберите статус' ,
				] ) ,
			] ,
			[
				'attribute' => 'relatedPromocode.serial' ,
				'format'    => 'text' ,
				'label'     => 'Промокод' ,
				'content'   => function ( $model , $key , $index , $column ) {

					return ( $model->promocode_id ) ? 'Да' : 'Нет';
				} ,
			] ,
			[
				'class'    => 'yii\grid\ActionColumn' ,
				'template' => $actions ,
				'buttons'  => [
					'call'  => function ( $url , $model ) {

						return Html::a ( '<span class="glyphicon glyphicon-earphone"></span>' , $url , [
							'title' => ( $model->relatedCustomer->fullName ) ? 'Набрать ' . $model->relatedCustomer->phone : 'Набрать ' . $model->relatedCustomer->phone ,
						] );
					} ,
					'print-order' => function ( $url , $model ) {

						return Html::a ( '<span class="glyphicon glyphicon-print"></span>' , $url , [
							'title' => 'Напечатать' ,
						] );
					},
				] ,
			] ,
		];
		 ?>
	<?= GridView::widget ( [
		'dataProvider' => $dataProvider ,
		'filterModel'  => $searchModel ,
		'columns'      => $gridcol ,

		
	] ); ?>
	<?php if(!isset($_GET['page'])){ ?>
	<script>
		setTimeout(function(){
			window.location.reload(1);
		}, 10000);
	</script>
	<?php } ?>
</div>
