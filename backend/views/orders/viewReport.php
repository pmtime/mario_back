<?php

	use yii\helpers\Html;
	use yii\helpers\VarDumper;
	use yii\widgets\DetailView;

	/* @var $this yii\web\View */
	/* @var $model common\models\Orders */

	$this->title = $this->context->labelOne;
	$this->params['breadcrumbs'][] = ['label' => $this->context->labelMany, 'url' => ['index']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-view">

	
	<div class="row">
		<div class="col-sm-6">
			<h4>Клиент</h4>
			<?= DetailView::widget([
				'model' => $model->getRelatedCustomer()->one(),
				'attributes' => [
					'fullName',
					'phone'
				],
			]) ?>
		</div>
		<div class="col-sm-6">
			<h4>Заказанные товары</h4>
			<?php foreach ($model->getViaGoods()->all() as $goods){ ?>
				<!--Product-->
				<?php $product = $goods->getRelatedGoods()->one(); ?>
				<!--Price-->
				<?php $price = $goods->getRelatedGoods()->one()->getRelatedPrice()->andFilterWhere(['id'=>$goods->price_id])->one(); ?>
				<!--Compliments | Depends from $product->id-->
				<?php $compliments = $model->getRelatedCompliments()->andFilterWhere(['good_id'=>$product->id])->all(); ?>
				<!--View-->
				<h5><?php echo $goods->getRelatedGood()->one()->label; ?></h5>
				<ul>
					<?php if($price->dia){ ?>
						<li>Диаметр: <?php echo $price->dia; ?></li>
					<?php }; ?>
					<?php if($goods->thickness){ ?>
						<li>Толщина: <?php echo ($goods->thickness == 1) ? 'Тонкая':'Обычная'; ?></li>
					<?php }; ?>
					<?php if($compliments){ ?>
						<li>Добавки:
							<ul>
								<?php foreach ($compliments as $compliment){ ?>
									<li><?php echo $compliment->getRelatedCompliment()->one()->label; ?> - кол-во: <?php echo $compliment->count; ?></li>
								<?php } ?>
							</ul>
						</li>
					<?php }; ?>
					<li>Кол-во: <?php echo $goods->count; ?></li>
				</ul>
			<?php } ?>
			<?php if($model->relatedPromocode){ ?>
				<?php if($model->relatedPromocode->goods){ ?>
					<h4>Товары по промокоду</h4>
					<?php foreach($model->relatedPromocode->relatedGoods as $promo_goods){ ?>
						<ul>
							<li><?php echo $promo_goods->relatedGood->label; ?> - кол-во: <?php echo $promo_goods->count; ?> шт.</li>
						</ul>
					<?php } ?>

				<?php } ?>
			<?php }; ?>
		</div>
	</div>


	<h4>Данные заказа</h4>
	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			[
				'attribute'=>'promocode_id',
				'value'=>($model->promocode_id) ? 'Да - '.$model->getRelatedPromocode()->one()->serial : 'Нет'
			],
			[
				'attribute'=>'delivery_id',
				'value'=>$model->getRelatedDelivery()->one()->label
			],
			[
				'attribute'=>'payment_id',
				'value'=>$model->getRelatedPayment()->one()->label
			],
			[
				'attribute'=>'create_time',
				'value'=>'Заказно в: '.$model->create_time.' | Обработано в: '.$model->update_time
			],
			'adres',
			'comment:ntext',
			'summary',
		],
	]) ?>

</div>
