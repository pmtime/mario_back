<?php
	use yii\helpers\Html;
	use metalguardian\formBuilder\ActiveFormBuilder;

	/* @var $this yii\web\View */
	/* @var $model common\models\Orders */
	/* @var $form \metalguardian\formBuilder\ActiveFormBuilder; */
?>

<div class="orders-form" id="app">

	<button id="recalculate">Пересчитать</button>
	<button id="clear-goods">Удалить все товары</button>
	<h1>Итого: <span id="sum"><?php echo (!$model->isNewRecord) ? $model->getSummary() : '0'; ?></span></h1>


	<?php $form = ActiveFormBuilder::begin (); ?>
	<?= $form->renderForm ( $model , $model->getFormConfig () ) ?>
	<div class="form-group">
		<?= Html::submitButton ( Yii::t ( 'app' , 'Save' ) , [ 'class' => 'btn btn-success' ] ) ?>
	</div>
	<?php ActiveFormBuilder::end (); ?>
</div>
