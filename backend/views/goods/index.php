<?php

	use common\models\Categories;
	use common\models\Goods;
	use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\editable\Editable;
use himiklab\sortablegrid\SortableGridView;
use yii\helpers\ArrayHelper;
	/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\GoodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $this->context->labelMany;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goods-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create ').$this->context->labelOne, ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'sortableAction' => 'sort',
        'columns' => [
            'id',
            [
	            'attribute'=>'category_id',
                'value'=>function($model){
	                return $model->getRelatedCategory()->one()->label;
                },
                'filter'          => Html::activeDropDownList ( $searchModel , 'category_id' , ArrayHelper::map ( Categories::find ()->asArray ()->all () , 'id' , 'label' ) , [
	            'class'  => 'form-control' ,
	            'prompt' => 'Выберите категорию',
            ] ) ,
            ],
            'label',
            /*[
	            'class'           => 'kartik\grid\EditableColumn' ,
	            'attribute'       => 'priority' ,
	            'value'           => 'priority' ,
	            'editableOptions' => [
		            'header'    => 'приоритет' ,
		            'inputType' => Editable::INPUT_DROPDOWN_LIST ,
		            'data'      => range(0,99) ,
		            'options'   => [
			            'pluginOptions' => [
				            'min' => 0 ,
				            'max' => 5000,
			            ],
		            ],
	            ] ,
	            'hAlign'          => 'left' ,
	            'vAlign'          => 'middle' ,
	            'width'           => '200px' ,
	            'format'          => 'text' ,
            ] ,*/
            [
	            'class' => 'yii\grid\ActionColumn',
	            'template'=>'{update}{delete}',
	            'buttons'=>[
		            'update' => function ($url, $model) {

			            return Html::a('
			            <span class="glyphicon glyphicon-pencil"></span>',
				            ($model->category_id == 4) ? ['goods/update-pizza','id'=>$model->id] : $url,
				            [
				            'title' => 'Редактировать'
			                ]
			            );
		            }
	            ] ,
	            ] ,
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
