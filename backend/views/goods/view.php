<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Goods */

$this->title = $this->context->labelOne;
$this->params['breadcrumbs'][] = ['label' => $this->context->labelMany, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goods-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'label',
            'category_id',
            'price_id',
            'thumbnail_base_url:url',
            'thumbnail_path',
            'attribute_id',
            'description:ntext',
            'author_id',
            'updater_id',
            'create_time',
            'update_time',
            'status_id',
        ],
    ]) ?>

</div>
