<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SmsSoloCustomer */

$this->title = Yii::t('app', 'Создание');
$this->params['breadcrumbs'][] = ['label' => $this->context->labelMany, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-solo-customer-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
