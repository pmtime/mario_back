<?php
use yii\helpers\Html;
use metalguardian\formBuilder\ActiveFormBuilder;

/* @var $this yii\web\View */
/* @var $model common\models\SmsSoloCustomer */
/* @var $form \metalguardian\formBuilder\ActiveFormBuilder; */
?>

<div class="sms-solo-customer-form">

<?php $form = ActiveFormBuilder::begin(); ?>
<?= $form->renderForm($model, $model->getFormConfig()) ?>
<div class="form-group">        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?></div>
<?php ActiveFormBuilder::end(); ?>
</div>
