$(function() {
    "use strict";

    //Make the dashboard widgets sortable Using jquery UI
    $(".connectedSortable").sortable({
        placeholder: "sort-highlight",
        connectWith: ".connectedSortable",
        handle: ".box-header, .nav-tabs",
        forcePlaceholderSize: true,
        zIndex: 999999
    }).disableSelection();
    $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");
});
$(document).on('click','#recalculate',function (e) {
    sendAjax($('#w0').serialize(),'/orders/recalculate');
});

function sendAjax($data,$href) {
    jQuery.ajax({'cache': false, 'type': 'POST', 'dataType': 'json', 'data':$data, 'success': function (response) {
        console.log(response);
        $('#sum').text(response['response']);
        $('#orders-summary').val(response['response']);
    }, 'error': function (response) {
        console.log('error')
    }, 'beforeSend': function() {

    }, 'complete': function() {

    }, 'url': $href});
    return false;
}
jQuery('#clear-goods').on('click', function(e) {
    $('#orders-relatedgoods').multipleInput('clear');
});

jQuery('#orders-relatedgoods').on('afterDeleteRow', function(e) {
    sendAjax($('#w0').serialize(),'/orders/recalculate');
});

function genereateAjaxIds() {
    var from = 0, to = 50;
    while(from <= to){
        addAjaxRow(from++);
    }

}

function addAjaxRow($id){
    $(document).on('change','#orders-relatedgoods-'+$id+'-good_id', function() {
        $.post( "good-dia?id="+$(this).val(), function( data ) {
            $( "#orders-relatedgoods-"+$id+"-price_id" ).html( data );
        });
    });
}
$(document).on('keyup','#orders-relatedgoods-0-count',function () {
    sendAjax($('#w0').serialize(),'/orders/recalculate');
});
genereateAjaxIds();


